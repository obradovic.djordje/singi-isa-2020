package rs.ac.singidunum.isa.app.repository;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Prenos;

@Repository
public interface PrenosRepository extends PagingAndSortingRepository<Prenos, Long> {
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public <S extends Prenos> S save(S entity);
}
