package rs.ac.singidunum.isa.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity
public class Racun {
	@Id
	@Column(length = 32)
	private String brojRacuna;
	@Column(nullable = false)
	private Double stanje;

	@ManyToOne(optional = false)
	private Korisnik vlasnik;
	
	@Version
	public long version;

	@OneToMany(mappedBy = "primalac", cascade = CascadeType.ALL)
	private List<Prenos> uplate = new ArrayList<Prenos>();

	@OneToMany(mappedBy = "uplatilac", cascade = CascadeType.ALL)
	private List<Prenos> isplate = new ArrayList<Prenos>();

	public Racun() {
		super();
	}

	public Racun(String brojRacuna, Double stanje) {
		super();
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public Korisnik getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Korisnik vlasnik) {
		this.vlasnik = vlasnik;
	}

	public List<Prenos> getUplate() {
		return uplate;
	}

	public void setUplate(List<Prenos> uplate) {
		this.uplate = uplate;
	}

	public List<Prenos> getIsplate() {
		return isplate;
	}

	public void setIsplate(List<Prenos> isplate) {
		this.isplate = isplate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brojRacuna == null) ? 0 : brojRacuna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Racun other = (Racun) obj;
		if (brojRacuna == null) {
			if (other.brojRacuna != null)
				return false;
		} else if (!brojRacuna.equals(other.brojRacuna))
			return false;
		return true;
	}
}
