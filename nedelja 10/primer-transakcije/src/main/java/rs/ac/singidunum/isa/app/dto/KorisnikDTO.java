package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.List;

public class KorisnikDTO {
	private Long id;
	private String korisnickoIme;
	private String ime;
	private String prezime;
	private List<RacunDTO> racuni = new ArrayList<RacunDTO>();
	public KorisnikDTO() {
		super();
	}
	public KorisnikDTO(Long id, String korisnickoIme, String ime, String prezime) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.ime = ime;
		this.prezime = prezime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public List<RacunDTO> getRacuni() {
		return racuni;
	}
	public void setRacuni(List<RacunDTO> racuni) {
		this.racuni = racuni;
	}
	
}
