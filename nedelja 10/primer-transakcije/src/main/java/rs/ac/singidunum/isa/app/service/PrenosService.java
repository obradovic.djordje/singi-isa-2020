package rs.ac.singidunum.isa.app.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.ac.singidunum.isa.app.model.Prenos;
import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.repository.PrenosRepository;
import rs.ac.singidunum.isa.app.repository.RacunRepository;

@Service
@Transactional
public class PrenosService {
	@Autowired
	private PrenosRepository prenosRepository;
	
	@Autowired
	private RacunRepository racunRepository;

	public PrenosService() {
		super();
	}
	
	/*
	 * Bez transakcija.
	 */
//	public void prenos(String brojRacunaUplatioca, String brojRacunaPrimaoca, Double iznos) throws Exception {
//		Optional<Racun> uplatilac = racunRepository.findById(brojRacunaUplatioca);
//		Optional<Racun> primalac = racunRepository.findById(brojRacunaPrimaoca);
//		if(uplatilac.isPresent() && primalac.isPresent()) {
//			uplatilac.get().setStanje(uplatilac.get().getStanje()-iznos);
//			primalac.get().setStanje(primalac.get().getStanje()+iznos);
//			racunRepository.save(uplatilac.get());
//			if(true) {
//				throw new Exception("Greska u toku izvrsavanja!");
//			}
//			racunRepository.save(primalac.get());
//			prenosRepository.save(new Prenos(null, uplatilac.get(), primalac.get(), LocalDateTime.now(), iznos));
//		} else {
//			
//		}
//	}
	
	/*
	 * Izvrsavanje kao transakcija.
	 */
	@Transactional
	public void prenos(String brojRacunaUplatioca, String brojRacunaPrimaoca, Double iznos) throws Exception {
		System.out.println("Pocetak: " + new Date());
		Optional<Racun> uplatilac = racunRepository.findById(brojRacunaUplatioca);
		Optional<Racun> primalac = racunRepository.findById(brojRacunaPrimaoca);
		if(uplatilac.isPresent() && primalac.isPresent()) {
			uplatilac.get().setStanje(uplatilac.get().getStanje()-iznos);
			primalac.get().setStanje(primalac.get().getStanje()+iznos);
			racunRepository.save(uplatilac.get());
			if (true) {
				throw new RuntimeException("Greska u toku izvrsavanja!");
			}
			racunRepository.save(primalac.get());
			prenosRepository.save(new Prenos(null, uplatilac.get(), primalac.get(), LocalDateTime.now(), iznos));
		} else {
			
		}
		System.out.println("Kraj: " + new Date());
	}
}
