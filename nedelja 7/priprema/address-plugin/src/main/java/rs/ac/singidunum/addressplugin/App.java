package rs.ac.singidunum.addressplugin;

import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import rs.ac.singidunum.addressplugin.plugin.PluginDescription;

@SpringBootApplication
public class App {
	public static void main(String[] args) {
		RestTemplate rt = new RestTemplate();
		// Kriranje opisa plugin-a.
		PluginDescription pd = new PluginDescription("Address plugin", "address", "Address service plugin",
				"http://localhost:8081", new HashMap<HttpMethod, HashMap<String, String>>());
		
		// Dodavanje spiska endpoint-a u opis plugin-a.
		pd.getEndpoints().put(HttpMethod.GET, new HashMap<String, String>());
		pd.getEndpoints().get(HttpMethod.GET).putIfAbsent("getAll", "/api/addresses");
		
		// Registracija plugin-a na host aplikaciju.
		rt.postForLocation("http://localhost:8080/api/plugins", pd);
		
		// Pokretanje plugin-a.
		SpringApplication.run(App.class, args);
	}
}
