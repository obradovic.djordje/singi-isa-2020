package rs.ac.singidunum.addressplugin.controllers;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.addressplugin.model.Address;

@Controller
@RequestMapping("/api/addresses")
public class AddressController {
	@RequestMapping
	public ResponseEntity<Iterable<Address>> getAddresses() {
		ArrayList<Address> addresses = new ArrayList<Address>();
		addresses.add(new Address("ULICA", "51B"));
		addresses.add(new Address("ULICA 2", "31B"));
		return new ResponseEntity<Iterable<Address>>(addresses, HttpStatus.OK);
	}
}
