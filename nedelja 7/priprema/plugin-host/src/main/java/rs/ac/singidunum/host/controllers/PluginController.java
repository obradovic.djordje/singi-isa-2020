package rs.ac.singidunum.host.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.host.plugin.PluginDescription;
import rs.ac.singidunum.host.plugin.PluginRepository;

/*
 * Kontroler koji vrsi obradu zahteva vezanih za upravljanje
 * plugin-ovima.
 */
@Controller
@RequestMapping("/api/plugins")
public class PluginController {
	@Autowired
	PluginRepository pr;
	
	/*
	 * Metoda koja vrsi registraciju novog plugin-a.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> registerPlugin(@RequestBody PluginDescription pluginDescription) {
		pr.registerPlugin(pluginDescription);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
