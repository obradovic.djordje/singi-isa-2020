package rs.ac.singidunum.host.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import rs.ac.singidunum.host.plugin.PluginRepository;

/*
 * Kontroler za rad sa adresama.
 */
@Controller
@RequestMapping("/api/addresses")
public class AddressController {
	@Autowired
	PluginRepository pr;
	
	@RequestMapping
	public ResponseEntity<Object> getAddresses() {
		/*
		 * Zahtev za dobavljanje svih adresa delegira se prvom
		 * registrovanom pluginu u kategoriji address. Ovom
		 * pluginu se upucuje get zahtev na endpoint pod nazivom
		 * getAll.
		 */
		return pr.getPlugins("address").get(0).sendRequest(HttpMethod.GET, "getAll");
	}
}
