package rs.ac.singidunum.host.plugin;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/*
 * Klasa cije instance predstavljaju dostupne plugin-ove.
 */
public class Plugin {
	private PluginDescription description;

	public Plugin() {
	}

	public Plugin(PluginDescription description) {
		this.description = description;
	}

	public PluginDescription getDescription() {
		return description;
	}

	public void setDescription(PluginDescription description) {
		this.description = description;
	}
	
	/*
	 * Metoda za slanje zahteva odredjenog tipa na odredjeni endpoint plugin-a.
	 * Endpoint se bira po nazivu endpointa.
	 * 
	 * Povratni tip metode je ResponseEntity koji u sebi sadrzi bilo koji objekat.
	 * Ovo omogucava kasniju upotrebu dobavljenog objekta u host aplikaciji.
	 */
	public ResponseEntity<Object> sendRequest(HttpMethod method, String targetEndpoint) {
		RestTemplate rt = new RestTemplate();
		return rt.getForEntity(description.getUrl() + description.getEndpointUrl(method, targetEndpoint), Object.class);
	}
}
