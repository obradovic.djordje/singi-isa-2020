package rs.ac.singidunum.host.plugin;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.stereotype.Repository;

/*
 * Skladiste svih registrovanih pluginova.
 */
@Repository
public class PluginRepository {
	private HashMap<String, ArrayList<Plugin>> pluginRepository = new HashMap<String, ArrayList<Plugin>>();
	
	public PluginRepository() {
	}
	
	/*
	 * Metoda za registraciju novog plugin-a.
	 * Plugin se dodaje u listu svih plugin-ova koji pripadaju istoj kategoriji.
	 */
	public void registerPlugin(PluginDescription pluginDescription) {
		if(pluginRepository.get(pluginDescription.getCategory()) == null) {
			pluginRepository.put(pluginDescription.getCategory(), new ArrayList<Plugin>());
		}
		Plugin plugin = new Plugin(pluginDescription);
		pluginRepository.get(pluginDescription.getCategory()).add(plugin);
	}
	
	/*
	 * Metoda za dobavljanje svih pluginova iz iste kategorije.
	 */
	public ArrayList<Plugin> getPlugins(String category) {
		return pluginRepository.get(category);
	}
}
