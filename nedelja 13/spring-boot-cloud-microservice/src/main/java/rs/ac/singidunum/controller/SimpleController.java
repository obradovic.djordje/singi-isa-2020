package rs.ac.singidunum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.model.Korisnik;
import rs.ac.singidunum.service.KorisnikService;

@Controller
public class SimpleController {
	@Value("${povratna.vrednost}")
	private String povratnaVrednost;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@RequestMapping(path = "/korisnici", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Page<Korisnik>> getAllKorisnici(Pageable pageable) {
		return new ResponseEntity<Page<Korisnik>>(korisnikService.findAll(pageable), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/simple", method = RequestMethod.GET)
	public ResponseEntity<String> getSimpleResult() {
		return new ResponseEntity<String>(povratnaVrednost, HttpStatus.OK);
	}
}
