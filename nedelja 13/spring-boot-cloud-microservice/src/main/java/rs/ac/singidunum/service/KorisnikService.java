package rs.ac.singidunum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.model.Korisnik;
import rs.ac.singidunum.repository.KorisnikRepository;

@Service
public class KorisnikService {
	@Autowired
	KorisnikRepository korisnikRepository;

	public KorisnikService() {
		super();
	}
	
	public Page<Korisnik> findAll(Pageable pageable) {
		return korisnikRepository.findAll(pageable);
	}
}
