package rs.ac.singidunum.isa.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Administrator extends Korisnik {
	@Column(nullable = false)
	private String email;

	public Administrator() {
		super();
	}

	public Administrator(Long id, String korisnickoIme, String lozinka, String ime, String prezime, String email) {
		super(id, korisnickoIme, lozinka, ime, prezime);
		this.email = email;
	}

	public Administrator(String email) {
		super();
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
