package rs.ac.singidunum.isa.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.repository.RacunRepository;

@Service
public class RacunService {
	@Autowired
	private RacunRepository racunRepository;
	
	public RacunService() {
	}
	
	public Iterable<Racun> findAll() {
		return racunRepository.findAll();
	}
	
	public Iterable<Racun> findAllByStanjeBetween(Double min, Double max) {
		return racunRepository.findAllWhereStanjeBetween(min, max);
	}
	
	public Racun findOne(String brojRacuna) {
		return racunRepository.findById(brojRacuna).orElse(null);
	}
	
	public void save(Racun racun) {
		racunRepository.save(racun);
	}
	
	public void delete(String brojRacuna) {
		racunRepository.deleteById(brojRacuna);
	}
	
	public void delete(Racun racun) {
		racunRepository.delete(racun);
	}
}
