package rs.ac.singidunum.isa.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.service.RacunService;

@Controller
@RequestMapping(path = "/api/racuni")
@CrossOrigin
public class RacunController {
	@Autowired
	private RacunService racunService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<Racun>> getAllRacuni() {
		return new ResponseEntity<List<Racun>>(racunService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.GET)
	public ResponseEntity<Racun> getRacun(@PathVariable("brojRacuna") String brojRacuna) {
		Racun racun = racunService.findOne(brojRacuna);
		if (racun != null) {
			return new ResponseEntity<Racun>(racun, HttpStatus.OK);
		}
		return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Racun> createRacun(@RequestBody Racun noviRacun) {
		if (racunService.findOne(noviRacun.getBrojRacuna()) != null) {
			return new ResponseEntity<Racun>(HttpStatus.CONFLICT);
		}
		racunService.save(noviRacun);
		return new ResponseEntity<Racun>(noviRacun, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.PUT)
	public ResponseEntity<Racun> updateRacun(@PathVariable("brojRacuna") String brojRacuna,
			@RequestBody Racun podaciRacuna) {
		Racun racun = racunService.findOne(brojRacuna);
		if (racun == null) {
			return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
		}
		racun.setStanje(podaciRacuna.getStanje());
		racunService.save(racun);
		return new ResponseEntity<Racun>(racun, HttpStatus.OK);
	}

	@RequestMapping(path = "/{brojRacuna}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteRacun(@PathVariable("brojRacuna") String brojRacuna) {	
		if (racunService.findOne(brojRacuna) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		racunService.delete(brojRacuna);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
