package rs.ac.singidunum.isa.app.dto;

public class PrenosDTO {
	private String uplatilac;
	private String primalac;
	private Double iznos;

	public PrenosDTO() {
		super();
	}

	public PrenosDTO(String uplatilac, String primalac, Double iznos) {
		super();
		this.uplatilac = uplatilac;
		this.primalac = primalac;
		this.iznos = iznos;
	}

	public String getUplatilac() {
		return uplatilac;
	}

	public void setUplatilac(String uplatilac) {
		this.uplatilac = uplatilac;
	}

	public String getPrimalac() {
		return primalac;
	}

	public void setPrimalac(String primalac) {
		this.primalac = primalac;
	}

	public Double getIznos() {
		return iznos;
	}

	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}

	@Override
	public String toString() {
		return "PrenosDTO [uplatilac=" + uplatilac + ", primalac=" + primalac + ", iznos=" + iznos + "]";
	}
	
}
