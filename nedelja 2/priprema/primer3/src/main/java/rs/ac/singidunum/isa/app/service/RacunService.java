package rs.ac.singidunum.isa.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.aspect.Logged;
import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.repository.RacunRepository;

@Service
public class RacunService {
	@Autowired
	private RacunRepository racunRepository;
	
	public RacunService() {
	}
	
	public List<Racun> findAll() {
		return racunRepository.findAll();
	}
	
	public Racun findOne(String brojRacuna) {
		return racunRepository.findOne(brojRacuna);
	}
	
	public void save(Racun racun) {
		racunRepository.save(racun);
	}
	
	public void delete(String brojRacuna) {
		racunRepository.delete(brojRacuna);
	}
	
	public void delete(Racun racun) {
		racunRepository.delete(racun);
	}
	
	@Logged
	public void prenos(String brojRacunaUplatioca, String brojRacunaPrimaoca, Double iznos) {
		Racun uplatilac = racunRepository.findOne(brojRacunaUplatioca);
		Racun primalac = racunRepository.findOne(brojRacunaPrimaoca);
		uplatilac.setStanje(uplatilac.getStanje()-iznos);
		primalac.setStanje(primalac.getStanje()+iznos);
		racunRepository.save(uplatilac);
		racunRepository.save(primalac);
	}
}
