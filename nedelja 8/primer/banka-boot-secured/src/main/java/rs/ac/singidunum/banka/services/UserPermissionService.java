package rs.ac.singidunum.banka.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.banka.model.UserPermission;
import rs.ac.singidunum.banka.repositories.UserPermissionRepository;

@Service
public class UserPermissionService {
	@Autowired
	private UserPermissionRepository userPermissionRepository;
	
	public Set<UserPermission> getPermissionsByUserId(Long id) {
		return userPermissionRepository.getByUserId(id);
	}
}
