import { Component } from '@angular/core';

import { webSocket } from 'rxjs/webSocket' // for RxJS 6, for v5 use Observable.webSocket

export class Message {

  constructor(
      public sender: string,
      public content: string,
      public isBroadcast = false,
  ) { }
}


class MessageBody{
  public username: string;
  public choice: boolean;

  constructor(){}
}

export class AppMessage{

  public sender:String;
  public receiver:String;
  public body:MessageBody;

  constructor(){}
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ws';

  private ws;
  private username = "";
  private choice = false;
  private lista:AppMessage[] = [];


  constructor() {
    this.ws = webSocket('ws://localhost:8080/ws');
    this.ws.subscribe(
       (msg)=>{
         this.lista.push(msg);
        },
       (err) => console.log(err),
       () => console.log('complete')
     );
  }


    sent(aChoice:boolean){
      let msg:AppMessage = new AppMessage();
      msg.body = {'username': this.username, 'choice': aChoice};
      msg.sender = 'sender';
      msg.receiver = 'receiver';
      this.ws.next(JSON.stringify(msg));
    }


}
