package sing.isa.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * Klasa koja predstavlja implementaciju
 * jednostavnog klijenta.
 */
public class Client {
	public static void main(String[] args) {
		// Petlja koja simulira 100 uzastopnih, nezavisnih, zahteva.
		for (int i = 0; i < 100; i++) {
			try {
				// Instanciranje socket-a za pristup serveru
				// na navedenoj adresi i portu.
				Socket clientSocket = new Socket("localhost", 3000);
				// PrintWriter za zapisivanje zahteva.
				PrintWriter os = new PrintWriter(clientSocket.getOutputStream());
				// BufferedReader za citanje sadrzaja odgovora primljenog od servera.
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				// Slanje zahteva serveru.
				os.println("PING");
				os.flush();
				// Ispis odgovora servera.
				System.out.println(in.readLine());
				// Zatvaranje stream-ova za pisanje i citanje.
				os.close();
				in.close();
				// Zatvaranje konekcije.
				clientSocket.close();
			} catch (UnknownHostException e) {
				// U slucaju neispravno zadatog hosta
				// ispisuje se stack trace i poruka.
				e.printStackTrace();
				System.out.println("Zadat nepostojeći host.");
			} catch (IOException e) {
				// U slucaju greske prilikom razmene podataka
				// ispsuje se stack trace i poruka.
				e.printStackTrace();
				System.out.println("Neuspešno povezivanje na server.");
			}
		}
	}
}
