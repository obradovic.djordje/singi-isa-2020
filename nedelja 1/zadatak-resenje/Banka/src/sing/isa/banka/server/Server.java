package sing.isa.banka.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/*
 * Implementacija servera koji prilikom obrade jednog
 * zahteva ne blokira obradu drugih zahteva.
 */
public class Server {
	public static void main(String[] args) {
		// Server cuva podatke jednog racuna.
		Racun racun = new Racun(10000);

		// Port na kojem server slusa.
		int port = 3000;
		// Otvaranje novog ServerSocket-a.
		try(ServerSocket ss = new ServerSocket(port)) {
			// Potvrda da je server uspesno pokrenut.
			System.out.println("Server slu�a na port: " + port);
			// Beskonacna petlja u kojoj server prihvata nove
			// konekcije od klijenata.
			while (true) {
				// Server prima novi zahtev i prihvata ga na obradu.
				Socket clientSocket = ss.accept();
				// Kreira se novi obradjivac zahteva za dobijenu konekciju.
				Handler h = new Handler(clientSocket, racun);
				// Kreiranje novog thread-a za pokretanje obradjivaca.
				Thread t = new Thread(h);
				// Pokretanje thread-a.
				t.start();
			}
		} catch (IOException e) {
			// U slucaju da dodje do neuspesnog instanciranja ServerSocket-a
			// ispisuje se stack trace i poruka o gresci.
			e.printStackTrace();
			System.out.println("Neuspe�no pokretanje servera.");
		}
	}

}
