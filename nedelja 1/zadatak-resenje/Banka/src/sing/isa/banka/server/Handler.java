package sing.isa.banka.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/*
 * Klasa koja predstavlja obradjivac zahteva.
 * Mora implementirati interfejs Runnable kako
 * bi se objekti ove klase mogli proslediti
 * thread-u na izvrsavanje.
 */
public class Handler implements Runnable {
	// Cuva referencu na prosledjeni socket
	// radi dalje obrade.
	private Socket socket;
	// Cuva referencu na prosledjeni racun
	// radi dalje obrade.
	private Racun racun;

	// Konstruktor koji prima referencu na socket
	// klijenta i racun.
	public Handler(Socket socket, Racun racun) {
		this.socket = socket;
		this.racun = racun;
	}

	@Override
	public void run() {
		try {
			// PrintWriter za zapisivanje odgovora.
			PrintWriter os = new PrintWriter(socket.getOutputStream());
			// BufferedReader za citanje sadrzaja primljene poruke.
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// Ispis adrese klijenta.
			System.out.println("Konekcija pristigla od: " + socket.getInetAddress().toString());
			// Citanje zahteva upucenog od klijenta.
			String input = in.readLine();

			// Sve dok klijent ne posalje zahtev STOP vrsi se obrada.
			while (!input.equals("STOP")) {
				// Ispis poruke o izmeni stanja.
				System.out.println("Izmena stanja: " + input);
				// Pretvaranje dobijenog sadrzaja zahteva u broj.
				double iznos = Double.parseDouble(input);

				// Ukoliko je zadati iznos manji od 0 i ako ima raspolozivih
				// sredstava vrsi se umanjivanje iznosa na racunu.
				if (iznos <= 0 && racun.getRaspolozivo() > 0) {
					// Vrsi se izmena stanja racuna.
					racun.setRaspolozivo(racun.getRaspolozivo() + iznos);
					// Salje se poruka klijentu da su sredstva oduzeta.
					os.println("Sredstva oduzeta");
					os.flush();
				// Ukoliko je zadati iznos veci od 0 i ako raspoloziva sredstva
				// manja od 100000 vrsi se dodavanje sredstava.
				} else if (iznos >= 0 && racun.getRaspolozivo() < 100000) {
					// Vrsi se izmena stanja racuna.
					racun.setRaspolozivo(racun.getRaspolozivo() + iznos);
					// Salje se poruka klijentu da su sredstva dodata.
					os.println("Sredstva dodata");
					os.flush();
				} else {
					// Ukoliko nije dobijen ispravan zahtev, klijentu
					// se salje poruka da transakcija nije uspela.
					os.println("Neuspela transakcija!");
					os.flush();
				}
				
				// Iscitava se sadrzaj novog zahteva.
				input = in.readLine();
			}

			// Zatvaranje tokova i socket-a.
			os.close();
			in.close();
			socket.close();

			// Ispis konacnog stanja racuna.
			System.out.println("Stanje: " + racun.getRaspolozivo());
		} catch (IOException e) {
			// U slucaju greske vrsi se ispis stack trace-a.
			e.printStackTrace();
		}

	}

}
