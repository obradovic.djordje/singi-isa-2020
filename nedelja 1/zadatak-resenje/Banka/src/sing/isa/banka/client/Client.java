package sing.isa.banka.client;

public class Client {
	public static void main(String[] args) {
		// Pokrece se 5 klijenata koji istovremeno
		// menjaju stanje servera, odnosno stanje racuna.
		for (int i = 0; i < 5; i++) {
			Handler h = new Handler();
			Thread t = new Thread(h);
			t.start();
		}
	}
}
