package rs.ac.singidunum.isa.app.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rs.ac.singidunum.isa.app.model.Racun;

public class RacunRepository {
	private ArrayList<Racun> racuni;

	public RacunRepository() {
		super();
		racuni = new ArrayList<Racun>();
		racuni.add(new Racun("231001230041230412", 1030.0));
		racuni.add(new Racun("123948192475920332", 3002.0));
		racuni.add(new Racun("662301294958012039", 6000.0));
	}
	
	public List<Racun> findAll() {
		return racuni;
	}
	
	public Racun findOne(String brojRacuna) {
		for(Racun r : racuni) {
			if(r.getBrojRacuna().equals(brojRacuna)) {
				return r;
			}
		}
		
		return null;
	}
	
	public void save(Racun racun) {
		Racun r = findOne(racun.getBrojRacuna());
		
		if(r == null) {
			racuni.add(racun);
		} else {
			r.setStajne(racun.getStajne());
		}
	}
	
	public void delete(String brojRacuna) {
		Iterator<Racun> iterator = racuni.iterator();
		while(iterator.hasNext()) {
			Racun r = iterator.next();
			if(r.getBrojRacuna().equals(brojRacuna)) {
				iterator.remove();
				return;
			}
		}
	}
	
	public void delete(Racun racun) {
		delete(racun.getBrojRacuna());
	}
}
