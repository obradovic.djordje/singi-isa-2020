package rs.ac.singidunum.isa.app.model;

public class Racun {
	private String brojRacuna;
	private Double stajne;
	
	public Racun() {
		super();
	}
	public Racun(String brojRacuna, Double stajne) {
		super();
		this.brojRacuna = brojRacuna;
		this.stajne = stajne;
	}
	public String getBrojRacuna() {
		return brojRacuna;
	}
	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}
	public Double getStajne() {
		return stajne;
	}
	public void setStajne(Double stajne) {
		this.stajne = stajne;
	}
}
