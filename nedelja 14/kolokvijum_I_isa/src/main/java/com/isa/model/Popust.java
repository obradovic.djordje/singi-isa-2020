package com.isa.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.dto.DTO;
import com.isa.dto.PopustDTO;

@Entity
@Table(name = "popust")
public class Popust implements ModelEntity<Popust, Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Long")
	@Column(name = "id")
	private Long id;

	@Type(type = "java.lang.String")
	@Column(name = "naziv")
	private String naziv;
	
//	@Type(type = "time")
	@Column(name = "pocetak")
	private LocalDateTime pocetak;
	
//	@Type(type = "time")
	@Column(name = "kraj")
	private LocalDateTime kraj;
	
//	@Type(type = "java.lang.Double")
	@Column(name = "procenat")
	private Double procenat;
	
	@ManyToMany(
			fetch = FetchType.LAZY,
			cascade = {
					CascadeType.DETACH,
					CascadeType.MERGE,
					CascadeType.PERSIST,
					CascadeType.REFRESH
			})
	@JoinTable(name = "artikal_popust",
				joinColumns = @JoinColumn(name = "popust_id"),
				inverseJoinColumns = @JoinColumn(name = "artikal_u_maloprodaji_id"))
	private List<ArtikalUMaloprodaji> artikliUMaloprodaji;
	
	public Popust() {
		super();
		this.artikliUMaloprodaji = new ArrayList<ArtikalUMaloprodaji>();
	}

	public Popust(String naziv, LocalDateTime pocetak, LocalDateTime kraj, Double procenat) {
		super();
		this.naziv = naziv;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.procenat = procenat;
		this.artikliUMaloprodaji = new ArrayList<ArtikalUMaloprodaji>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getPocetak() {
		return pocetak;
	}

	public void setPocetak(LocalDateTime pocetak) {
		this.pocetak = pocetak;
	}

	public LocalDateTime getKraj() {
		return kraj;
	}

	public void setKraj(LocalDateTime kraj) {
		this.kraj = kraj;
	}

	public Double getProcenat() {
		return procenat;
	}

	public void setProcenat(Double procenat) {
		this.procenat = procenat;
	}

	public List<ArtikalUMaloprodaji> getArtikliUMaloprodaji() {
		return artikliUMaloprodaji;
	}

	public void setArtikliUMaloprodaji(List<ArtikalUMaloprodaji> artikliUMaloprodaji) {
		this.artikliUMaloprodaji = artikliUMaloprodaji;
	}
	
	public boolean addArtikalUMaloprodaji(ArtikalUMaloprodaji artikalUMaloprodaji) {
		this.artikliUMaloprodaji.add(artikalUMaloprodaji);		
		return true;
	}
	
	@Override
	public String toString() {
		return "Popust [id=" + id + ", naziv=" + naziv + ", pocetak=" + pocetak + ", kraj=" + kraj + ", procenat="
				+ procenat + ", artikliUMaloprodaji=" + artikliUMaloprodaji + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artikliUMaloprodaji == null) ? 0 : artikliUMaloprodaji.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((kraj == null) ? 0 : kraj.hashCode());
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((pocetak == null) ? 0 : pocetak.hashCode());
		result = prime * result + ((procenat == null) ? 0 : procenat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Popust other = (Popust) obj;
		if (artikliUMaloprodaji == null) {
			if (other.artikliUMaloprodaji != null)
				return false;
		} else if (!artikliUMaloprodaji.equals(other.artikliUMaloprodaji))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (kraj == null) {
			if (other.kraj != null)
				return false;
		} else if (!kraj.equals(other.kraj))
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (pocetak == null) {
			if (other.pocetak != null)
				return false;
		} else if (!pocetak.equals(other.pocetak))
			return false;
		if (procenat == null) {
			if (other.procenat != null)
				return false;
		} else if (!procenat.equals(other.procenat))
			return false;
		return true;
	}
	
//	interfaces

	@Override
	public void update(Popust t) {
		this.setKraj(t.getKraj());
		this.setPocetak(t.getPocetak());
		this.setNaziv(t.getNaziv());
		this.setProcenat(t.getProcenat());
		
		for (ArtikalUMaloprodaji artikalUMaloprodaji : t.getArtikliUMaloprodaji()) {
			this.addArtikalUMaloprodaji(artikalUMaloprodaji);
			artikalUMaloprodaji.getPopusti().add(this);
		}
	}

	@Override
	public DTO getDTO() {
		
		List<DTO> a = this.artikliUMaloprodaji.stream().map(t -> t.getDTO()).collect(Collectors.toList());
		
		return new PopustDTO(id, naziv, pocetak, kraj, procenat, a);
	}

}
