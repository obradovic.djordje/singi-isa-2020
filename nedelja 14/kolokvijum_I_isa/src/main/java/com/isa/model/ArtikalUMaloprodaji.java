package com.isa.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.dto.ArtikalUMaloporodajiDTO;
import com.isa.dto.DTO;
import com.isa.dto.PopustDTO;
import com.isa.dto.StavkaRacunaDTO;

@Entity
@Table(name = "artikal_u_maloprodaji")
public class ArtikalUMaloprodaji implements ModelEntity<ArtikalUMaloprodaji, Long> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Long")
	@Column(name = "id")
	private Long id;
	
//	@Type(type = "java.lang.Double")
	@Column(name = "cena")
	private Double cena;
	
//	@Type(type = "java.lang.Double")
	@Column(name = "kolicina")
	private Double kolicina;
	
	@ManyToMany(
			fetch = FetchType.LAZY,
			cascade = {
					CascadeType.DETACH,
					CascadeType.MERGE,
					CascadeType.PERSIST,
					CascadeType.REFRESH
			})
	@JoinTable(name = "artikal_popust",
				joinColumns = @JoinColumn(name = "artikal_u_maloprodaji_id"),
				inverseJoinColumns = @JoinColumn(name = "popust_id"))
	private List<Popust> popusti;
	
	@OneToMany(mappedBy = "artikalUMaloprodaji",
				fetch = FetchType.LAZY,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				})
	private List<StavkaRacuna> stavkeRacuna;
	
	public ArtikalUMaloprodaji() {
		super();
		this.popusti = new ArrayList<Popust>();
		this.stavkeRacuna = new ArrayList<StavkaRacuna>();
	}

	public ArtikalUMaloprodaji(Double cena, Double kolicina) {
		super();
		this.cena = cena;
		this.kolicina = kolicina;
		this.popusti = new ArrayList<Popust>();
		this.stavkeRacuna = new ArrayList<StavkaRacuna>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Double getKolicina() {
		return kolicina;
	}

	public void setKolicina(Double kolicina) {
		this.kolicina = kolicina;
	}

	public List<Popust> getPopusti() {
		return popusti;
	}

	public void setPopusti(List<Popust> popusti) {
		this.popusti = popusti;
	}	
	
	public boolean addPopust(Popust popust) {
		this.popusti.add(popust);
		return popust.addArtikalUMaloprodaji(this);
	}
	
	public List<StavkaRacuna> getStavkeRacuna() {
		return stavkeRacuna;
	}

	public void setStavkeRacuna(List<StavkaRacuna> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}
	
	public boolean addStavkaRacuna(StavkaRacuna stavkaRacuna) {
		this.stavkeRacuna.add(stavkaRacuna);
		stavkaRacuna.setArtikalUMaloprodaji(this);
		return true;
	}

	@Override
	public String toString() {
		return "ArtikalUMaloprodaji [id=" + id + ", cena=" + cena + ", kolicina=" + kolicina + ", popusti=" + popusti
				+ ", stavkeRacuna=" + stavkeRacuna + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cena == null) ? 0 : cena.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
		result = prime * result + ((popusti == null) ? 0 : popusti.hashCode());
		result = prime * result + ((stavkeRacuna == null) ? 0 : stavkeRacuna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtikalUMaloprodaji other = (ArtikalUMaloprodaji) obj;
		if (cena == null) {
			if (other.cena != null)
				return false;
		} else if (!cena.equals(other.cena))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (kolicina == null) {
			if (other.kolicina != null)
				return false;
		} else if (!kolicina.equals(other.kolicina))
			return false;
		if (popusti == null) {
			if (other.popusti != null)
				return false;
		} else if (!popusti.equals(other.popusti))
			return false;
		if (stavkeRacuna == null) {
			if (other.stavkeRacuna != null)
				return false;
		} else if (!stavkeRacuna.equals(other.stavkeRacuna))
			return false;
		return true;
	}
	
	
//	interfaces

	@Override
	public void update(ArtikalUMaloprodaji t) {
		this.setCena(t.getCena());
		this.setKolicina(t.getKolicina());
		
		for (Popust popust : t.getPopusti()) {
			this.addPopust(popust);
		}
		
		for (StavkaRacuna stavkaRacuna : t.getStavkeRacuna()) {
			this.addStavkaRacuna(stavkaRacuna);
		}
	}

	@Override
	public DTO getDTO() {
		
		List<PopustDTO> p = this.popusti.stream().map(t -> (PopustDTO)t.getDTO()).collect(Collectors.toList());
		List<StavkaRacunaDTO> s = this.stavkeRacuna.stream().map(t -> (StavkaRacunaDTO)t.getDTO()).collect(Collectors.toList());
		
		return new ArtikalUMaloporodajiDTO(id, cena, kolicina, p, s);
	}

}
