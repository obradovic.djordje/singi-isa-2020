package com.isa.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.isa.dto.DTO;
import com.isa.dto.RacunDTO;

@Entity
@Table(name = "racun")
public class Racun implements ModelEntity<Racun, Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "java.lang.Long")
	@Column(name = "id")
	private Long id;
//	
//	@Type(type = "time")
	@Column(name = "datum_izdavanja")
	private LocalDateTime datumIzdavanja;
	
	@OneToMany(mappedBy = "racun",
				fetch = FetchType.LAZY,
				cascade = {
						CascadeType.DETACH,
						CascadeType.MERGE,
						CascadeType.PERSIST,
						CascadeType.REFRESH
				})
	private List<StavkaRacuna> stavkeRacuna;

	public Racun() {
		super();
		this.stavkeRacuna = new ArrayList<StavkaRacuna>();
	}

	public Racun(LocalDateTime datumIzdavanja) {
		super();
		this.datumIzdavanja = datumIzdavanja;
		this.stavkeRacuna = new ArrayList<StavkaRacuna>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDatumIzdavanja() {
		return datumIzdavanja;
	}

	public void setDatumIzdavanja(LocalDateTime datumIzdavanja) {
		this.datumIzdavanja = datumIzdavanja;
	}
	
	public List<StavkaRacuna> getStavkeRacuna() {
		return stavkeRacuna;
	}

	public void setStavkeRacuna(List<StavkaRacuna> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}
	
	public boolean addStavkaRacuna(StavkaRacuna stavkaRacuna) {
		this.stavkeRacuna.add(stavkaRacuna);
		stavkaRacuna.setRacun(this);
		return true;
	}
	
	@Override
	public String toString() {
		return "Racun [id=" + id + ", datumIzdavanja=" + datumIzdavanja + ", stavkeRacuna=" + stavkeRacuna + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datumIzdavanja == null) ? 0 : datumIzdavanja.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((stavkeRacuna == null) ? 0 : stavkeRacuna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Racun other = (Racun) obj;
		if (datumIzdavanja == null) {
			if (other.datumIzdavanja != null)
				return false;
		} else if (!datumIzdavanja.equals(other.datumIzdavanja))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (stavkeRacuna == null) {
			if (other.stavkeRacuna != null)
				return false;
		} else if (!stavkeRacuna.equals(other.stavkeRacuna))
			return false;
		return true;
	}
	
//	interfaces
	
	@Override
	public void update(Racun t) {
		this.setDatumIzdavanja(t.getDatumIzdavanja());
		
		for (StavkaRacuna stavkaRacuna : t.getStavkeRacuna()) {
			this.addStavkaRacuna(stavkaRacuna);
		}
	}

	@Override
	public DTO getDTO() {

		List<DTO> s = this.stavkeRacuna.stream().map(t -> t.getDTO()).collect(Collectors.toList());
		
		return new RacunDTO(id, datumIzdavanja, s);
	}

}
