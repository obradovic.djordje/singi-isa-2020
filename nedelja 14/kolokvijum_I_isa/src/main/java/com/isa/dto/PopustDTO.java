package com.isa.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PopustDTO extends DTO {

	private Long id;

	private String naziv;
	
	private LocalDateTime pocetak, kraj;
	
	private Double procenat;
	
	private List<DTO> artikliUMaloprodaji;

	public PopustDTO() {
		super();
		this.artikliUMaloprodaji = new ArrayList<DTO>();
	}

	public PopustDTO(Long id, String naziv, LocalDateTime pocetak, LocalDateTime kraj, Double procenat,
			List<DTO> artikliUMaloprodaji) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.procenat = procenat;
		this.artikliUMaloprodaji = artikliUMaloprodaji;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getPocetak() {
		return pocetak;
	}

	public void setPocetak(LocalDateTime pocetak) {
		this.pocetak = pocetak;
	}

	public LocalDateTime getKraj() {
		return kraj;
	}

	public void setKraj(LocalDateTime kraj) {
		this.kraj = kraj;
	}

	public Double getProcenat() {
		return procenat;
	}

	public void setProcenat(Double procenat) {
		this.procenat = procenat;
	}

	public List<DTO> getArtikliUMaloprodaji() {
		return artikliUMaloprodaji;
	}

	public void setArtikliUMaloprodaji(List<DTO> artikliUMaloprodaji) {
		this.artikliUMaloprodaji = artikliUMaloprodaji;
	}

	@Override
	public String toString() {
		return "PopustDTO [id=" + id + ", naziv=" + naziv + ", pocetak=" + pocetak + ", kraj=" + kraj + ", procenat="
				+ procenat + ", artikliUMaloprodaji=" + artikliUMaloprodaji + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artikliUMaloprodaji == null) ? 0 : artikliUMaloprodaji.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((kraj == null) ? 0 : kraj.hashCode());
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((pocetak == null) ? 0 : pocetak.hashCode());
		result = prime * result + ((procenat == null) ? 0 : procenat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PopustDTO other = (PopustDTO) obj;
		if (artikliUMaloprodaji == null) {
			if (other.artikliUMaloprodaji != null)
				return false;
		} else if (!artikliUMaloprodaji.equals(other.artikliUMaloprodaji))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (kraj == null) {
			if (other.kraj != null)
				return false;
		} else if (!kraj.equals(other.kraj))
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (pocetak == null) {
			if (other.pocetak != null)
				return false;
		} else if (!pocetak.equals(other.pocetak))
			return false;
		if (procenat == null) {
			if (other.procenat != null)
				return false;
		} else if (!procenat.equals(other.procenat))
			return false;
		return true;
	}
	
}
