package com.isa.dto;

import java.util.ArrayList;
import java.util.List;

public class ArtikalUMaloporodajiDTO extends DTO {
	
	private Long id;
	
	private Double cena, kolicina;
	
	private List<PopustDTO> popusti;
	
	private List<StavkaRacunaDTO> stavkeRacuna;

	public ArtikalUMaloporodajiDTO() {
		super();
		this.popusti = new ArrayList<PopustDTO>();
		this.stavkeRacuna = new ArrayList<StavkaRacunaDTO>();
	}

	public ArtikalUMaloporodajiDTO(Long id, Double cena, Double kolicina, List<PopustDTO> popusti,
			List<StavkaRacunaDTO> stavkeRacuna) {
		super();
		this.id = id;
		this.cena = cena;
		this.kolicina = kolicina;
		this.popusti = popusti;
		this.stavkeRacuna = stavkeRacuna;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Double getKolicina() {
		return kolicina;
	}

	public void setKolicina(Double kolicina) {
		this.kolicina = kolicina;
	}

	public List<PopustDTO> getPopusti() {
		return popusti;
	}

	public void setPopusti(List<PopustDTO> popusti) {
		this.popusti = popusti;
	}

	public List<StavkaRacunaDTO> getStavkeRacuna() {
		return stavkeRacuna;
	}

	public void setStavkeRacuna(List<StavkaRacunaDTO> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}

	@Override
	public String toString() {
		return "ArtikalUMaloporodajiDTO [id=" + id + ", cena=" + cena + ", kolicina=" + kolicina + ", popusti="
				+ popusti + ", stavkeRacuna=" + stavkeRacuna + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cena == null) ? 0 : cena.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
		result = prime * result + ((popusti == null) ? 0 : popusti.hashCode());
		result = prime * result + ((stavkeRacuna == null) ? 0 : stavkeRacuna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtikalUMaloporodajiDTO other = (ArtikalUMaloporodajiDTO) obj;
		if (cena == null) {
			if (other.cena != null)
				return false;
		} else if (!cena.equals(other.cena))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (kolicina == null) {
			if (other.kolicina != null)
				return false;
		} else if (!kolicina.equals(other.kolicina))
			return false;
		if (popusti == null) {
			if (other.popusti != null)
				return false;
		} else if (!popusti.equals(other.popusti))
			return false;
		if (stavkeRacuna == null) {
			if (other.stavkeRacuna != null)
				return false;
		} else if (!stavkeRacuna.equals(other.stavkeRacuna))
			return false;
		return true;
	}
	
}
