package com.isa.dto;

public class StavkaRacunaDTO extends DTO {
	
	private Long id;
	
	private Double kolicina, cena;
	
	private RacunDTO racun;
	
	private ArtikalUMaloporodajiDTO artikalUMaloprodaji;

	public StavkaRacunaDTO() {
		super();
	}

	public StavkaRacunaDTO(Long id, Double kolicina, Double cena, RacunDTO racun,
			ArtikalUMaloporodajiDTO artikalUMaloprodaji) {
		super();
		this.id = id;
		this.kolicina = kolicina;
		this.cena = cena;
		this.racun = racun;
		this.artikalUMaloprodaji = artikalUMaloprodaji;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getKolicina() {
		return kolicina;
	}

	public void setKolicina(Double kolicina) {
		this.kolicina = kolicina;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public RacunDTO getRacun() {
		return racun;
	}

	public void setRacun(RacunDTO racun) {
		this.racun = racun;
	}

	public ArtikalUMaloporodajiDTO getArtikalUMaloprodaji() {
		return artikalUMaloprodaji;
	}

	public void setArtikalUMaloprodaji(ArtikalUMaloporodajiDTO artikalUMaloprodaji) {
		this.artikalUMaloprodaji = artikalUMaloprodaji;
	}

	@Override
	public String toString() {
		return "StavkaRacunaDTO [id=" + id + ", kolicina=" + kolicina + ", cena=" + cena + ", racun=" + racun
				+ ", artikalUMaloprodaji=" + artikalUMaloprodaji + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artikalUMaloprodaji == null) ? 0 : artikalUMaloprodaji.hashCode());
		result = prime * result + ((cena == null) ? 0 : cena.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
		result = prime * result + ((racun == null) ? 0 : racun.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StavkaRacunaDTO other = (StavkaRacunaDTO) obj;
		if (artikalUMaloprodaji == null) {
			if (other.artikalUMaloprodaji != null)
				return false;
		} else if (!artikalUMaloprodaji.equals(other.artikalUMaloprodaji))
			return false;
		if (cena == null) {
			if (other.cena != null)
				return false;
		} else if (!cena.equals(other.cena))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (kolicina == null) {
			if (other.kolicina != null)
				return false;
		} else if (!kolicina.equals(other.kolicina))
			return false;
		if (racun == null) {
			if (other.racun != null)
				return false;
		} else if (!racun.equals(other.racun))
			return false;
		return true;
	}
	
}
