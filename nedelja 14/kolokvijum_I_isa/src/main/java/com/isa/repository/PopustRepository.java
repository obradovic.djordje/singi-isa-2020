package com.isa.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.isa.model.Popust;

@Repository
@Scope("singleton")
public class PopustRepository extends CrudRepository<Popust, Long> {

}
