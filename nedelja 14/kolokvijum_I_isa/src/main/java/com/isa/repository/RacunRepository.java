package com.isa.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.isa.model.Racun;

@Repository
@Scope("singleton")
public class RacunRepository extends CrudRepository<Racun, Long> {

}
