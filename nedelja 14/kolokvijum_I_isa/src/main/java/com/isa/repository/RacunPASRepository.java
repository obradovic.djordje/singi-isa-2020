package com.isa.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.isa.model.Racun;

@Repository
@Scope("singleton")
public interface RacunPASRepository extends PagingAndSortingRepository<Racun, Long> {

}
