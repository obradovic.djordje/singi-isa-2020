package com.isa.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.isa.model.Popust;

@Repository
@Scope("singleton")
public interface PopustPASRepository extends PagingAndSortingRepository<Popust, Long> {

}
