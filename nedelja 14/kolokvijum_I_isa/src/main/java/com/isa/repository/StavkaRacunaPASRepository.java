package com.isa.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.isa.model.StavkaRacuna;

@Repository
@Scope("singleton")
public interface StavkaRacunaPASRepository extends PagingAndSortingRepository<StavkaRacuna, Long> {

}
