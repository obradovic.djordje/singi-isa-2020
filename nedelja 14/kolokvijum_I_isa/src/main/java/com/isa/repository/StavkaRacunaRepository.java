package com.isa.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.isa.model.StavkaRacuna;

@Repository
@Scope("singleton")
public class StavkaRacunaRepository extends CrudRepository<StavkaRacuna, Long> {

}
