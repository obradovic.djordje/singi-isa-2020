package com.isa.hbsf;

import javax.annotation.PreDestroy;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.isa.model.ArtikalUMaloprodaji;
import com.isa.model.Popust;
import com.isa.model.Racun;
import com.isa.model.StavkaRacuna;

@Component
@Scope("singleton")
public class HibernateSessionFactory {
	private SessionFactory sessionFactory;

	public HibernateSessionFactory() {
		super();
		this.sessionFactory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(ArtikalUMaloprodaji.class)
				.addAnnotatedClass(StavkaRacuna.class)
				.addAnnotatedClass(Racun.class)
				.addAnnotatedClass(Popust.class)
				.buildSessionFactory();
		
//		get session
		Session session = this.sessionFactory.getCurrentSession();
		
//		begin transaction
		session.beginTransaction();
		
//		-------------------- CRUD --------------------------------------------------
		
//		commit transaction
		session.getTransaction().commit();
		
//		close session
		session.close();
	}
	
	public Session getSession() {
		return this.sessionFactory.getCurrentSession();
	}
	
	@PreDestroy
	public void onDestroy() {
		System.out.println("HibernateSessionFactory bean destroyed");
		this.sessionFactory.close();
	}

}
