package com.isa.service;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.isa.model.ArtikalUMaloprodaji;
import com.isa.repository.ArtikalUMaloprodajiRepository;

@Service
@Scope("singleton")
public class ArtikalUMaloprodajiService extends CrudService<ArtikalUMaloprodaji, Long> {
	
	public List<ArtikalUMaloprodaji> findAllOnDiscount() {
		return ((ArtikalUMaloprodajiRepository)this.repository).findAllOnDiscount();
	}

}
