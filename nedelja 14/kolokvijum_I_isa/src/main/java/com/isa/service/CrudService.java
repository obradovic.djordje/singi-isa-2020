package com.isa.service;

import java.io.Serializable;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.isa.model.ModelEntity;
import com.isa.repository.CrudRepository;

public abstract class CrudService<T extends ModelEntity<T, ID>, ID extends Serializable> {

	@Autowired
	protected CrudRepository<T, ID> repository;
	
//	on init
	@PostConstruct
	public void onInit() {
		System.out.println(this.getClass().getName() + " init");
	}
	
//	on destroy
	@PreDestroy
	public void onDestroy() {
		System.out.println(this.getClass().getName() + " destroyed");
	}
	
//	READ
//	find all pageable
	public Page<T> findAll(Pageable pageable) {
		return this.repository.findAll(pageable);
	}
	
//	find all
	public Iterable<T> findAll() {
		return this.repository.findAll();
	}
	
//	find one
	public Optional<T> findOne(ID id) {
		return this.repository.findOne(id);
	}
	
//	CREATE and UPDATE
	public void save(T model) {
		this.repository.save(model);
	}
	
//	DELETE
//	delete by id
	public void delete(ID id) {
		this.repository.delete(id);
	}
	
//	delete by model
	public void delete(T model) {
		this.repository.delete(model);
	}
	
}
