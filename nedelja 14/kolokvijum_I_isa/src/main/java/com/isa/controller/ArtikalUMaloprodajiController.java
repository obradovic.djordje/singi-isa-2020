package com.isa.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.isa.dto.ArtikalUMaloporodajiDTO;
import com.isa.model.ArtikalUMaloprodaji;
import com.isa.service.ArtikalUMaloprodajiService;

@Controller
@Scope("singleton")
@CrossOrigin(origins = "*")
@RequestMapping(path = "/api/artikli_u_maloprodaji")
public class ArtikalUMaloprodajiController extends CrudController<ArtikalUMaloprodaji, Long> {

	@RequestMapping(path = "/na-popustu", method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {
		
		((ArtikalUMaloprodajiService)this.service).findAllOnDiscount();
		
		List<ArtikalUMaloporodajiDTO> naPopustu = ((ArtikalUMaloprodajiService)this.service).findAllOnDiscount().stream().map(t -> (ArtikalUMaloporodajiDTO)t.getDTO()).collect(Collectors.toList());
		
		return new ResponseEntity<List<ArtikalUMaloporodajiDTO>>(naPopustu, HttpStatus.OK);
	}
	
}
