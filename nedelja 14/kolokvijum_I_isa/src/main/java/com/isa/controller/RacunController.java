package com.isa.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import com.isa.model.Racun;

@Controller
@Scope("singleton")
@CrossOrigin(origins = "*")
@RequestMapping(path = "/api/racuni")
public class RacunController extends CrudController<Racun, Long> {

}
