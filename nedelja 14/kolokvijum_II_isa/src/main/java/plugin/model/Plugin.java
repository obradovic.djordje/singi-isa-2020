package plugin.model;

import java.util.Iterator;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import kolokvijum.host.components.RestTemplateResponseErrorHandler;
import kolokvijum.model.interfaces.Entitet;

/*
 * Klasa cije instance predstavljaju dostupne plugin-ove.
 */
public class Plugin {
    private PluginDescription description;

    public Plugin() {
    }

    public Plugin(PluginDescription description) {
	this.description = description;
    }

    public PluginDescription getDescription() {
	return description;
    }

    public void setDescription(PluginDescription description) {
	this.description = description;
    }

    /*
     * Metoda za slanje zahteva odredjenog tipa na odredjeni endpoint plugin-a.
     * Endpoint se bira po nazivu endpointa.
     * 
     * Povratni tip metode je ResponseEntity koji u sebi sadrzi bilo koji objekat.
     * Ovo omogucava kasniju upotrebu dobavljenog objekta u host aplikaciji.
     */
//    findAll (Read)
    public ResponseEntity<?> findAll(String category, HttpMethod method, String targetEndpoint, Pageable pageable,
	    HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);

//	return rt.getForEntity(this.getUriString(category, method, targetEndpoint, "", pageable), Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, "", pageable), HttpMethod.GET, request,
		Object.class);
    }

//  findOne (Read)
    public ResponseEntity<?> findOne(String category, HttpMethod method, String targetEndpoint, String id,
	    HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);

//	return rt.getForEntity(this.getUriString(category, method, targetEndpoint, id, null), Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, id, null), HttpMethod.GET, request,
		Object.class);
    }

//    create (Create)
    public ResponseEntity<?> create(String category, HttpMethod method, String targetEndpoint, Entitet<?, ?> model,
	    HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	/**
	 * pri instanciranju HttpEntity klase nudi izmedju ostalog konstruktor koji
	 * prima body i header-e
	 */
	HttpEntity<Object> request = new HttpEntity<Object>(model, httpHeaders);

//	return rt.postForEntity(this.getUriString(category, method, targetEndpoint, "", null), model, Object.class);
	return rt.exchange(this.getUriString(category, method, targetEndpoint, "", null), HttpMethod.POST, request,
		Object.class);
    }

//  update (Update)
    public void update(String category, HttpMethod method, String targetEndpoint, Entitet<?, ?> model,
	    HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	/**
	 * pri instanciranju HttpEntity klase nudi izmedju ostalog konstruktor koji
	 * prima body i header-e
	 */
	HttpEntity<Object> request = new HttpEntity<Object>(model, httpHeaders);

//	rt.put(this.getUriString(category, method, targetEndpoint, "", null), model);
	rt.exchange(this.getUriString(category, method, targetEndpoint, "", null), HttpMethod.PUT, request,
		Object.class);
    }

//  delete by id (Delete)
    public void delete(String category, HttpMethod method, String targetEndpoint, String id, HttpHeaders httpHeaders) {
	RestTemplate rt = this.getRestTemplateWithCustomErrorHandling();

	HttpEntity<Object> request = new HttpEntity<Object>(httpHeaders);

//	rt.delete(this.getUriString(category, method, targetEndpoint, id, null));
	rt.exchange(this.getUriString(category, method, targetEndpoint, id, null), HttpMethod.DELETE, request,
		Object.class);
    }

    private RestTemplate getRestTemplateWithCustomErrorHandling() {
	RestTemplateBuilder builder = new RestTemplateBuilder();
	RestTemplate rt = builder.errorHandler(new RestTemplateResponseErrorHandler()).build();
	return rt;
    }

    private String getUriString(String category, HttpMethod method, String targetEndpoint, String id,
	    Pageable pageable) {
	UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme(description.getScheme())
		.host(description.getHost()).path(description.getEndpointUrl(category, method, targetEndpoint) + id);

	if (pageable != null) {
	    builder.queryParam("page", pageable.getPageNumber()).queryParam("size", pageable.getPageSize());

	    if (pageable.getSort().isSorted()) {
		Iterator<Sort.Order> iterator = pageable.getSort().iterator();
		while (iterator.hasNext()) {
		    Sort.Order sort = iterator.next();
		    String sortValue = sort.getProperty();
		    if (sort.isDescending())
			sortValue += ",desc";
		    else
			sortValue += ",asc";

//			System.out.println(sort.toString());
//			System.out.println(sort.getProperty());
//			System.out.println(sort.isDescending());

		    builder.queryParam("sort", sortValue);
		}
	    }
	}

	UriComponents uriComponents = builder.build();

	return uriComponents.toUriString();
    }

}
