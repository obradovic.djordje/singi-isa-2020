package kolokvijum.host.controller.studentskaSluzba;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import kolokvijum.host.controller.HostCrudController;
import kolokvijum.model.studentskaSluzba.Transakcija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/transakcija")
@CrossOrigin(origins = "*")

public class TransakcijaController extends HostCrudController<Transakcija, Long> {

    public TransakcijaController() {
	this.category = "transakcija";
    }

}