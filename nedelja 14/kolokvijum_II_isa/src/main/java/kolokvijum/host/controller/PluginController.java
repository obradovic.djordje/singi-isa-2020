package kolokvijum.host.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolokvijum.host.repository.PluginRepository;
import plugin.model.PluginDescription;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/plugins")

public class PluginController {

    @Autowired
    private PluginRepository pr;

    public PluginRepository getPr() {
	return this.pr;
    }

    public void setPr(PluginRepository pr) {
	this.pr = pr;
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<Object> registerPlugin(@RequestBody PluginDescription pluginDescription) {
	pr.registerPlugin(pluginDescription);
	return new ResponseEntity<Object>(HttpStatus.OK);
    }

}