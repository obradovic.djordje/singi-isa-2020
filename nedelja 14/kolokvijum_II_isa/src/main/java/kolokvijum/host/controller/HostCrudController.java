package kolokvijum.host.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolokvijum.host.repository.PluginRepository;
import kolokvijum.model.interfaces.Entitet;
import plugin.model.Plugin;

public abstract class HostCrudController<T extends Entitet<T, ID>, ID extends Serializable> {

    @Autowired
    protected PluginRepository pr;

    protected String category;

    public PluginRepository getPr() {
	return this.pr;
    }

    public void setPr(PluginRepository pr) {
	this.pr = pr;
    }

    public String getCategory() {
	return this.category;
    }

    public void setCategory(String category) {
	this.category = category;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<?> findAll(Pageable pageable, @RequestHeader HttpHeaders httpHeaders) {
	// http://localhost:8080/api/adresa?page=0&size=2&sort=tip,neki_drugi_property,desc&sort=id

	/*
	 * Zahtev za dobavljanje svih adresa delegira se prvom registrovanom pluginu u
	 * kategoriji address. Ovom pluginu se upucuje get zahtev na endpoint pod
	 * nazivom getAll.
	 */
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.findAll(this.category, HttpMethod.GET, "findAll", pageable, httpHeaders);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findOne(@PathVariable("id") ID id, @RequestHeader HttpHeaders httpHeaders) {

	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.findOne(this.category, HttpMethod.GET, "findOne", id.toString(), httpHeaders);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody T model, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.create(this.category, HttpMethod.POST, "create", model, httpHeaders);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody T model, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	plugin.update(this.category, HttpMethod.PUT, "update", model, httpHeaders);
	return plugin.findOne(this.category, HttpMethod.GET, "findOne", model.getId().toString(), httpHeaders);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") ID id, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	plugin.delete(this.category, HttpMethod.DELETE, "delete", id.toString(), httpHeaders);

//	tu zbog logickog brisanja ako je implementirano
//	return plugin.findOne(this.category, HttpMethod.GET, "findOne", id.toString(), httpHeaders);

	return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@RequestBody T model, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	plugin.delete(this.category, HttpMethod.DELETE, "delete", model.getId().toString(), httpHeaders);

//	tu zbog logickog brisanja ako je implementirano
//	return plugin.findOne(this.category, HttpMethod.GET, "findOne", model.getId().toString(), httpHeaders);

	return new ResponseEntity<Void>(HttpStatus.OK);
    }

}