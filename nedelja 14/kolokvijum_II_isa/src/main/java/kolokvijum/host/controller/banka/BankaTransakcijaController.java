package kolokvijum.host.controller.banka;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import kolokvijum.host.controller.HostCrudController;
import kolokvijum.model.banka.Transakcija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/banka_transakcija")
@CrossOrigin(origins = "*")

public class BankaTransakcijaController extends HostCrudController<Transakcija, Long> {

    public BankaTransakcijaController() {
	this.category = "banka_transakcija";
    }

}