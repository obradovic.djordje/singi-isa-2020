package kolokvijum.host.controller.studentskaSluzba;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolokvijum.host.controller.HostCrudController;
import kolokvijum.model.studentskaSluzba.Student;
import plugin.model.Plugin;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/student")
@CrossOrigin(origins = "*")

public class StudentController extends HostCrudController<Student, Long> {

    public StudentController() {
	this.category = "student";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody Student model, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.create(this.category, HttpMethod.POST, "register", model, httpHeaders);
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody Student model, @RequestHeader HttpHeaders httpHeaders) {
	Plugin plugin = pr.getPlugins(this.category).get(0);
	return plugin.create(this.category, HttpMethod.POST, "login", model, httpHeaders);
    }

}