package kolokvijum.host.controller.studentskaSluzba;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import kolokvijum.host.controller.HostCrudController;
import kolokvijum.model.studentskaSluzba.FinansijskaKartica;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/finansijska_kartica")
@CrossOrigin(origins = "*")

public class FinansijskaKarticaController extends HostCrudController<FinansijskaKartica, Long> {

    public FinansijskaKarticaController() {
	this.category = "finansijska_kartica";
    }

}