package kolokvijum.host.controller.banka;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import kolokvijum.host.controller.HostCrudController;
import kolokvijum.model.banka.Racun;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/racun")
@CrossOrigin(origins = "*")

public class RacunController extends HostCrudController<Racun, Long> {

    public RacunController() {
	this.category = "racun";
    }

}