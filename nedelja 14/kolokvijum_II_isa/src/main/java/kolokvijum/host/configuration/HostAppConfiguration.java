package kolokvijum.host.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration

public class HostAppConfiguration {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource("/kolokvijum/host/configuration/host.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}