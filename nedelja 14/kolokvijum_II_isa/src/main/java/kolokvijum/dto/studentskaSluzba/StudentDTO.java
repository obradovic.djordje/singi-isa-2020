package kolokvijum.dto.studentskaSluzba;

import kolokvijum.dto.interfaces.DTO;

public class StudentDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 382252178426246194L;

    private Long id;

    private String indeks;

    private String ime;

    private String prezime;

    private String email;

    private String lozinka;

    private FinansijskaKarticaDTO finansijskaKarticaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getIndeks() {
	return this.indeks;
    }

    public void setIndeks(String indeks) {
	this.indeks = indeks;
    }

    public String getIme() {
	return this.ime;
    }

    public void setIme(String ime) {
	this.ime = ime;
    }

    public String getPrezime() {
	return this.prezime;
    }

    public void setPrezime(String prezime) {
	this.prezime = prezime;
    }

    public String getEmail() {
	return this.email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getLozinka() {
	return this.lozinka;
    }

    public void setLozinka(String lozinka) {
	this.lozinka = lozinka;
    }

    public FinansijskaKarticaDTO getFinansijskaKarticaDTO() {
	return this.finansijskaKarticaDTO;
    }

    public void setFinansijskaKarticaDTO(FinansijskaKarticaDTO finansijskaKarticaDTO) {
	this.finansijskaKarticaDTO = finansijskaKarticaDTO;
    }

    public StudentDTO() {
	super();
    }

}