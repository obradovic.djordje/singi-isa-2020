package kolokvijum.dto.studentskaSluzba;

import kolokvijum.dto.interfaces.DTO;

public class FinansijskaKarticaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -5150150985369616697L;

    private Long id;

    private String pozivNaBroj;

    private Double stanje;

    private StudentDTO studentDTO;

    private java.util.List<TransakcijaDTO> transakcijeDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getPozivNaBroj() {
	return this.pozivNaBroj;
    }

    public void setPozivNaBroj(String pozivNaBroj) {
	this.pozivNaBroj = pozivNaBroj;
    }

    public Double getStanje() {
	return this.stanje;
    }

    public void setStanje(Double stanje) {
	this.stanje = stanje;
    }

    public StudentDTO getStudentDTO() {
	return this.studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
	this.studentDTO = studentDTO;
    }

    public java.util.List<TransakcijaDTO> getTransakcijeDTO() {
	return this.transakcijeDTO;
    }

    public void setTransakcijeDTO(java.util.List<TransakcijaDTO> transakcijeDTO) {
	this.transakcijeDTO = transakcijeDTO;
    }

    public FinansijskaKarticaDTO() {
	super();
	this.transakcijeDTO = new java.util.ArrayList<>();
    }

}