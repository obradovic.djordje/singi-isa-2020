package kolokvijum.dto.studentskaSluzba;

import java.time.LocalDateTime;

import kolokvijum.dto.interfaces.DTO;

public class TransakcijaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -6414405307288334560L;

    private Long id;

    private Double iznos;

    private LocalDateTime datumValute;

    private FinansijskaKarticaDTO finansijskaKarticaDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getIznos() {
	return this.iznos;
    }

    public void setIznos(Double iznos) {
	this.iznos = iznos;
    }

    public LocalDateTime getDatumValute() {
	return this.datumValute;
    }

    public void setDatumValute(LocalDateTime datumValute) {
	this.datumValute = datumValute;
    }

    public FinansijskaKarticaDTO getFinansijskaKarticaDTO() {
	return this.finansijskaKarticaDTO;
    }

    public void setFinansijskaKarticaDTO(FinansijskaKarticaDTO finansijskaKarticaDTO) {
	this.finansijskaKarticaDTO = finansijskaKarticaDTO;
    }

    public TransakcijaDTO() {
	super();
    }

}