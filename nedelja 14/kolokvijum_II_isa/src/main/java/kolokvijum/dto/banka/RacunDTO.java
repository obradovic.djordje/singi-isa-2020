package kolokvijum.dto.banka;

import kolokvijum.dto.interfaces.DTO;

public class RacunDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -537084938478893990L;

    private Long id;

    private String brojRacuna;

    private Double stanje;

    private java.util.List<TransakcijaDTO> transakcijeDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getBrojRacuna() {
	return this.brojRacuna;
    }

    public void setBrojRacuna(String brojRacuna) {
	this.brojRacuna = brojRacuna;
    }

    public Double getStanje() {
	return this.stanje;
    }

    public void setStanje(Double stanje) {
	this.stanje = stanje;
    }

    public java.util.List<TransakcijaDTO> getTransakcijeDTO() {
	return this.transakcijeDTO;
    }

    public void setTransakcijeDTO(java.util.List<TransakcijaDTO> transakcijeDTO) {
	this.transakcijeDTO = transakcijeDTO;
    }

    public RacunDTO() {
	super();
	this.transakcijeDTO = new java.util.ArrayList<>();
    }

}