package kolokvijum.dto.banka;

import java.time.LocalDateTime;

import kolokvijum.dto.interfaces.DTO;

public class TransakcijaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 709675129697043497L;

    private Long id;

    private Double iznos;

    private LocalDateTime datumValute;

    private String pozivNaBroj;

    private RacunDTO racunDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getIznos() {
	return this.iznos;
    }

    public void setIznos(Double iznos) {
	this.iznos = iznos;
    }

    public LocalDateTime getDatumValute() {
	return this.datumValute;
    }

    public void setDatumValute(LocalDateTime datumValute) {
	this.datumValute = datumValute;
    }

    public String getPozivNaBroj() {
	return this.pozivNaBroj;
    }

    public void setPozivNaBroj(String pozivNaBroj) {
	this.pozivNaBroj = pozivNaBroj;
    }

    public RacunDTO getRacunDTO() {
	return this.racunDTO;
    }

    public void setRacunDTO(RacunDTO racunDTO) {
	this.racunDTO = racunDTO;
    }

    public TransakcijaDTO() {
	super();
    }

}