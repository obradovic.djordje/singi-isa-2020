package kolokvijum.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class BankTransaction implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7348926030078717472L;
    private Double iznos;
    private LocalDateTime datumValute;
    private String pozivNaBroj;

    public Double getIznos() {
	return iznos;
    }

    public void setIznos(Double iznos) {
	this.iznos = iznos;
    }

    public LocalDateTime getDatumValute() {
	return datumValute;
    }

    public void setDatumValute(LocalDateTime datumValute) {
	this.datumValute = datumValute;
    }

    public String getPozivNaBroj() {
	return pozivNaBroj;
    }

    public void setPozivNaBroj(String pozivNaBroj) {
	this.pozivNaBroj = pozivNaBroj;
    }

    public BankTransaction(Double iznos, LocalDateTime datumValute, String pozivNaBroj) {
	super();
	this.iznos = iznos;
	this.datumValute = datumValute;
	this.pozivNaBroj = pozivNaBroj;
    }

}
