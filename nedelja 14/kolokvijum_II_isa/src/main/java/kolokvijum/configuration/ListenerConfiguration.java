package kolokvijum.configuration;

import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@Configuration
public class ListenerConfiguration {
//    @Autowired
//    ConnectionFactory connectionFactory;
//
//    /*
//     * Definisanje bean-a za topic listener fabriku.
//     */
//    @Bean
//    public DefaultJmsListenerContainerFactory jmsTopicListenerContainerFactory(
//	    DefaultJmsListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
//	DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
//	configurer.configure(factory, connectionFactory);
//
//	/**
//	 * po meni factory.setPubSubDomain(true); ce uciniti da ova fabrika proizvede
//	 * JmsTemplate za topic
//	 * 
//	 * inace imamo i default-ni JmsListener za queue (koji nam nudi Spring Boot)
//	 * kojem nismo u application.properties dodali spring.jms.pub-sub-domain=true,
//	 * jer bi nam onda taj default-ni bio za topic (malo je ovo sada cudno jer je
//	 * Ivan dole definisao bean i za default-ni JmsListener)
//	 */
//	factory.setPubSubDomain(true);
//	return factory;
//    }
//
//    /*
//     * Definisanje jms template bean-a za queue poruke. (Default-ni JmsListener)
//     */
//    @Bean
//    public JmsTemplate jmsTemplate() {
//	JmsTemplate template = new JmsTemplate(connectionFactory);
//	/**
//	 * setujemo template.setPubSubDomain(false); na false, jer je fabrika podesena
//	 * da proizvodi JmsTemplate za topic
//	 */
//	template.setPubSubDomain(false);
//	return template;
//    }
//
//    /*
//     * Definisanje jms template bean-a za topic poruke.
//     */
//    @Bean
//    public JmsTemplate jmsTopicTemplate() {
//	JmsTemplate template = new JmsTemplate(connectionFactory);
//	/**
//	 * setujemo template.setPubSubDomain(true); na true, za svaki slucaj, iako je
//	 * fabrika podesena da proizvodi JmsTemplate za topic
//	 */
//	template.setPubSubDomain(true);
//	return template;
//    }
    
    
    @Autowired
    ConnectionFactory connectionFactory;

    /*
     * Definisanje bean-a za queue listener fabriku.
     */
    @Bean
    public DefaultJmsListenerContainerFactory jmsQueueListenerContainerFactory(
	    DefaultJmsListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
	DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
	configurer.configure(factory, connectionFactory);

	/**
	 * po meni factory.setPubSubDomain(false); ce uciniti da ova fabrika proizvede
	 * JmsTemplate za queue
	 */
	factory.setPubSubDomain(false);
	return factory;
    }
    
    /*
     * Definisanje bean-a za topic listener fabriku.
     */
    @Bean
    public DefaultJmsListenerContainerFactory jmsTopicListenerContainerFactory(
	    DefaultJmsListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
	DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
	configurer.configure(factory, connectionFactory);

	/**
	 * po meni factory.setPubSubDomain(true); ce uciniti da ova fabrika proizvede
	 * JmsTemplate za topic
	 * 
	 * inace imamo i default-ni JmsListener za queue (koji nam nudi Spring Boot)
	 * kojem nismo u application.properties dodali spring.jms.pub-sub-domain=true,
	 * jer bi nam onda taj default-ni bio za topic (malo je ovo sada cudno jer je
	 * Ivan dole definisao bean i za default-ni JmsListener)
	 */
	factory.setPubSubDomain(true);
	return factory;
    }

    /*
     * Definisanje jms template bean-a za queue poruke. (Default-ni JmsTemplate)
     */
    @Bean
    public JmsTemplate jmsTemplate() {
	JmsTemplate template = new JmsTemplate(connectionFactory);
	/**
	 * setujemo template.setPubSubDomain(false); na false, za svaki slucaj, iako je
	 * fabrika(connectionFactory) podesena da proizvodi JmsTemplate za queue
	 */
	template.setPubSubDomain(false);
	return template;
    }

    /*
     * Definisanje jms template bean-a za topic poruke.
     */
    @Bean
    public JmsTemplate jmsTopicTemplate() {
	JmsTemplate template = new JmsTemplate(connectionFactory);
	/**
	 * setujemo template.setPubSubDomain(true); na true, jer je fabrika(connectionFactory) podesena
	 * da proizvodi JmsTemplate za queue
	 */
	template.setPubSubDomain(true);
	return template;
    }
}
