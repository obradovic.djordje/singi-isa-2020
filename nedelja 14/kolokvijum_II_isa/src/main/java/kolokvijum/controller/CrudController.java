package kolokvijum.controller;

import java.io.Serializable;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolokvijum.dto.interfaces.DTO;
import kolokvijum.model.interfaces.Entitet;
import kolokvijum.service.CrudService;

public abstract class CrudController<T extends Entitet<T, ID>, ID extends Serializable> {

    @Autowired
    protected CrudService<T, ID> service;

    public CrudService<T, ID> getService() {
	return this.service;
    }

    public void setService(CrudService<T, ID> service) {
	this.service = service;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Page<DTO<ID>>> findAll(Pageable pageable) {
	System.out.println("Pageable: " + pageable); // Page request [number: 0, size 20, sort: UNSORTED]

//	get Page<T>
	Page<T> page = this.service.findAll(pageable);

//	map T to DTO and get Page<DTO>
	Page<DTO<ID>> pageDTO = page.map(t -> t.getDTO());

	return new ResponseEntity<Page<DTO<ID>>>(pageDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findOne(@PathVariable("id") ID id) {
	Optional<T> t = this.service.findOne(id);

	if (t.isPresent())
	    return new ResponseEntity<DTO<ID>>(t.get().getDTO(), HttpStatus.OK);
	else
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody T model) {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isEmpty()) {
//			we didn't find model, lets create one and return DTO
		this.service.save(model);
		return new ResponseEntity<DTO<ID>>(model.getDTO(), HttpStatus.CREATED);
	    } else {
//			model is found, lets return HttpStatus.CONFLICT
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	    }
	} else {
//		id doesn't exist, so model doesn't exist lets create one and return DTO
	    this.service.save(model);
	    return new ResponseEntity<DTO<ID>>(model.getDTO(), HttpStatus.CREATED);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody T model) {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets update it and return DTO
		t.get().update(model);
		this.service.save(t.get());
		return new ResponseEntity<DTO<ID>>(t.get().getDTO(), HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") ID id) {
//	try to find model
	Optional<T> t = this.service.findOne(id);

	if (t.isPresent()) {
//		model is found, lets delete it and return HttpStatus.OK
	    this.service.delete(t.get());
	    return new ResponseEntity<Void>(HttpStatus.OK);
	} else {
//		we didn't find model, lets return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@RequestBody T model) {
//	check for id
	if (model.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.service.findOne(model.getId());

	    if (t.isPresent()) {
//			model is found, lets delete it and return HttpStatus.OK
		this.service.delete(t.get());
		return new ResponseEntity<Void>(HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

}