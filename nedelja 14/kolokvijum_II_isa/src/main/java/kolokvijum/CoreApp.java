package kolokvijum;

import kolokvijum.host.HostApp;
import kolokvijum.plugin.banka.PluginBankaApp;
import kolokvijum.plugin.studentskaSluzba.PluginStudentskaSluzbaApp;

public class CoreApp {

    public static void main(String args[]) {
//	host
	HostApp.main(args);

//	plugins
	PluginBankaApp.main(args);
	PluginStudentskaSluzbaApp.main(args);
    }

}