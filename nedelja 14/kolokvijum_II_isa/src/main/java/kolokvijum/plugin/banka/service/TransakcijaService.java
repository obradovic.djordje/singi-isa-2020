package kolokvijum.plugin.banka.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import kolokvijum.dto.BankTransaction;
import kolokvijum.model.banka.Racun;
import kolokvijum.model.banka.Transakcija;
import kolokvijum.service.CrudService;

@Service
@Scope("singleton")

public class TransakcijaService extends CrudService<Transakcija, Long> {
    
    @Autowired
    private JmsTemplate jmsTemplate; // Podrazumevani jmsTemplate koji obezbedjuje Spring Boot.

    @Autowired
    private JmsTemplate jmsTopicTemplate; // Jms tempalte podesen za topic poruke
    
    @Autowired
    private RacunService racunService;
    
    @Transactional
    public void save(Transakcija model) {
	this.repository.save(model);
	
	Optional<Racun> racun = this.racunService.findOne(model.getRacun().getId());
	if(racun.isPresent()) {
	    Racun r = racun.get();
	    r.setStanje(r.getStanje() + model.getIznos());
	    
	    racunService.save(r);
	    
	    this.jmsTemplate.convertAndSend("izvrsenaTransakcija", new BankTransaction(model.getIznos(), model.getDatumValute(), model.getPozivNaBroj()));
	} else {
	    throw new RuntimeException("racun nije pronadjen");
	}
	
    }

}