package kolokvijum.plugin.banka.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;


import kolokvijum.service.CrudService;

import kolokvijum.model.banka.Racun;

@Service
@Scope("singleton")

public class RacunService extends CrudService<Racun, Long> {

}