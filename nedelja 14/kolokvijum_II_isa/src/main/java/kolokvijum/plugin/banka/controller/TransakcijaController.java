package kolokvijum.plugin.banka.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import kolokvijum.controller.CrudController;
import kolokvijum.model.banka.Transakcija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/banka_transakcija")
@CrossOrigin(origins = "*")

public class TransakcijaController extends CrudController<Transakcija, Long> {

}