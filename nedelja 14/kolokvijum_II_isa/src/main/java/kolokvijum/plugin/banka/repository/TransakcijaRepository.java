package kolokvijum.plugin.banka.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import kolokvijum.model.banka.Transakcija;
import kolokvijum.repository.CrudRepository;

@Repository
@Scope("singleton")

public class TransakcijaRepository extends CrudRepository<Transakcija, Long> {

}