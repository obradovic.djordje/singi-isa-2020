package kolokvijum.plugin.banka;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.web.client.RestTemplate;

import plugin.model.PluginDescription;

@SpringBootApplication
@ComponentScan(basePackages = { "kolokvijum.configuration", "kolokvijum.repository", "kolokvijum.service", "kolokvijum.controller",
	"kolokvijum.hbsf", "kolokvijum.plugin.banka", "kolokvijum.plugin.banka.*" })
@EntityScan(basePackages = { "kolokvijum.model.banka" })
@EnableJpaRepositories("kolokvijum.plugin.banka.repository")
@EnableJms
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class })
public class PluginBankaApp extends SpringBootServletInitializer {

    public static void main(String args[]) {
	RestTemplate rt = new RestTemplate();

//	kreiramo opis plugin-a
	PluginDescription pluginDescription = new PluginDescription("Banka", "Plugin za logicku celinu banka", "http",
		"localhost:8101", new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>());

//	dodajemo kategorije koje plugin pruza
	ArrayList<String> categories = new ArrayList<String>();
	categories.add("racun");
	categories.add("banka_transakcija");
	pluginDescription.setCategories(categories);

	for (String category : categories) {
//		dodajemo spisak endpoint-a u opis plugin-a
	    pluginDescription.getEndpoints().put(category, new HashMap<HttpMethod, HashMap<String, String>>());
	    pluginDescription.getEndpoints().get(category).put(HttpMethod.GET, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findAll",
		    "/api/" + category);
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findOne",
		    "/api/" + category + "/");

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.POST, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.POST).putIfAbsent("create",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.PUT, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.PUT).putIfAbsent("update",
		    "/api/" + category);

	    pluginDescription.getEndpoints().get(category).put(HttpMethod.DELETE, new HashMap<String, String>());
	    pluginDescription.getEndpoints().get(category).get(HttpMethod.DELETE).putIfAbsent("delete",
		    "/api/" + category + "/");
	}

//	registracija plugin-a na host-u
	rt.postForLocation("http://localhost:8080/api/plugins", pluginDescription);

//	pokretanje plugin-a
	SpringApplication.run(PluginBankaApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	return builder.sources(PluginBankaApp.class);
    }

}