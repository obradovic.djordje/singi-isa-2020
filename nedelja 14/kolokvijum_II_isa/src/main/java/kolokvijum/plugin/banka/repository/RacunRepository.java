package kolokvijum.plugin.banka.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;


import kolokvijum.repository.CrudRepository;

import kolokvijum.model.banka.Racun;

@Repository
@Scope("singleton")

public class RacunRepository extends CrudRepository<Racun, Long> {

}