package kolokvijum.plugin.banka.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokvijum.model.banka.Transakcija;

@Repository
@Scope("singleton")

public interface TransakcijaPASRepository extends PagingAndSortingRepository<Transakcija, Long> {

}