package kolokvijum.plugin.banka.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration

public class PluginBankaAppConfiguration {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(
		new ClassPathResource("/kolokvijum/plugin/banka/configuration/plugin.banka.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}