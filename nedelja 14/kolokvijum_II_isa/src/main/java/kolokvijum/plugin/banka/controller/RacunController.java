package kolokvijum.plugin.banka.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import kolokvijum.controller.CrudController;
import kolokvijum.model.banka.Racun;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/racun")
@CrossOrigin(origins = "*")

public class RacunController extends CrudController<Racun, Long> {

}