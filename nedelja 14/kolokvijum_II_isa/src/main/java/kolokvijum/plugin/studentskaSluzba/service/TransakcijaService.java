package kolokvijum.plugin.studentskaSluzba.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import kolokvijum.model.studentskaSluzba.Transakcija;
import kolokvijum.service.CrudService;

@Service
@Scope("singleton")

public class TransakcijaService extends CrudService<Transakcija, Long> {

}