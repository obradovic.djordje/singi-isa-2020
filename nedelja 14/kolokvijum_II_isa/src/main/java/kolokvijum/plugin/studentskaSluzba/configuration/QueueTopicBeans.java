package kolokvijum.plugin.studentskaSluzba.configuration;

import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueTopicBeans {
    
    @Bean
    public javax.jms.Queue izvrsenaTransakcija () {
	return new ActiveMQQueue("izvrsenaTransakcija");
    }
    
//    @Bean
//    public Topic requestReturned () {
//	return new ActiveMQTopic("requestReturned");
//    }

}
