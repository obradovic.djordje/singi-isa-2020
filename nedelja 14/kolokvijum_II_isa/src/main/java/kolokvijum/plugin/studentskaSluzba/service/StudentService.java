package kolokvijum.plugin.studentskaSluzba.service;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import kolokvijum.model.studentskaSluzba.Student;
import kolokvijum.model.studentskaSluzba.StudentRola;
import kolokvijum.service.CrudService;

@Service
@Scope("singleton")

public class StudentService extends CrudService<Student, Long> implements UserDetailsService {
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
	Optional<Student> student = this.findOne(Long.parseLong(id));

	if (student.isPresent()) {
	    /**
	     * buduci da u UserDetails interfejsu imamo username, password i listu
	     * GrantedAuthority-a(interfejs koji ima metodu getAuthority koja vraca String),
	     */
	    ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
	    for (StudentRola studentRola : student.get().getStudentRole()) {
		/**
		 * kreiramo instancu klase SimpleGrantedAuthority koja implemetira
		 * GrantedAuthority interfejs i kao argument prima role tipa String
		 */
		grantedAuthorities.add(new SimpleGrantedAuthority(studentRola.getRola().getTitle()));
	    }

	    /**
	     * kreiramo instancu klase User, koja implmentira interfejs UserDetails i kao
	     * argumente prima username, password i listu GrantedAuthority-a, koju smo
	     * prethodno kreirali
	     */
	    return new org.springframework.security.core.userdetails.User(student.get().getId().toString(),
		    student.get().getLozinka(), grantedAuthorities);
	}

	return null;
    }
}