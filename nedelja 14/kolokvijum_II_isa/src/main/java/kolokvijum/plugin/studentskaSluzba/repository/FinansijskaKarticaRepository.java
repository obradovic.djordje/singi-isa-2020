package kolokvijum.plugin.studentskaSluzba.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import kolokvijum.model.studentskaSluzba.FinansijskaKartica;
import kolokvijum.repository.CrudRepository;

@Repository
@Scope("singleton")

public class FinansijskaKarticaRepository extends CrudRepository<FinansijskaKartica, Long> {

}