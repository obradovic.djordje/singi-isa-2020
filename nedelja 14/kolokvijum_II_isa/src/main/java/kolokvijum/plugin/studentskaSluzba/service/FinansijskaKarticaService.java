package kolokvijum.plugin.studentskaSluzba.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import kolokvijum.model.studentskaSluzba.FinansijskaKartica;
import kolokvijum.service.CrudService;

@Service
@Scope("singleton")

public class FinansijskaKarticaService extends CrudService<FinansijskaKartica, Long> {

}