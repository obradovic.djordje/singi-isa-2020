package kolokvijum.plugin.studentskaSluzba.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokvijum.model.studentskaSluzba.FinansijskaKartica;

@Repository
@Scope("singleton")

public interface FinansijskaKarticaPASRepository extends PagingAndSortingRepository<FinansijskaKartica, Long> {

}