package kolokvijum.plugin.studentskaSluzba.controller;

import java.util.HashMap;
import java.util.HashSet;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolokvijum.controller.CrudController;
import kolokvijum.dto.studentskaSluzba.StudentDTO;
import kolokvijum.hbsf.HibernateSessionFactory;
import kolokvijum.model.studentskaSluzba.Rola;
import kolokvijum.model.studentskaSluzba.Student;
import kolokvijum.model.studentskaSluzba.StudentRola;
import kolokvijum.plugin.studentskaSluzba.service.StudentService;
import kolokvijum.plugin.studentskaSluzba.utils.TokenUtils;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/student")
@CrossOrigin(origins = "*")

public class StudentController extends CrudController<Student, Long> {
    @Autowired
    private StudentService studentService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private HibernateSessionFactory hibernateSessionFactory;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<StudentDTO> register(@RequestBody Student student) {

	student.setLozinka(passwordEncoder.encode(student.getLozinka()));

	this.service.save(student);
	student.setStudentRole(new HashSet<StudentRola>());

//	get session
	Session session = this.hibernateSessionFactory.getSession();
//	begin transaction
	session.beginTransaction();

	student.getStudentRole().add(new StudentRola(null, student, session.get(Rola.class, 1l)));

//	commit transaction
	session.getTransaction().commit();

//	close session
	session.close();

	this.service.save(student);

	return new ResponseEntity<StudentDTO>(student.getDTO(), HttpStatus.OK);
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, String>> login(@RequestBody Student student) {
	try {
	    /**
	     * kreiramo specificniju autentifikaciju(UsernamePasswordAuthenticationToken,
	     * koja implementira interfejs -> Authentication)
	     */
	    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
		    student.getId().toString(), student.getLozinka());

	    /**
	     * authenticationManager(AuthenticationManager) sluzi da autentifikuje neku
	     * autentifikaciju koju mu mi prosledimo
	     * 
	     * vraca najopstiji oblik, Authentication, autentifikacije u koje sve
	     * implementacije(konkretnije implmentacije interfejs-a Authentication) mogu da
	     * se svrstaju
	     */
	    Authentication authentication = authenticationManager.authenticate(token);
	    /**
	     * SecurityContextHolder je vezan za thread, mi stoga dohvatamo njegov sadrzaj i
	     * setujemo nasu autentifikaciju koju je vratio authenticationManager
	     * 
	     * SecurityContextHolder podesen je da radi na globalnom nivou zbog anotacije
	     * (@EnableGlobalMethodSecurity(securedEnabled = true) u
	     * rs.ac.singidunum.banka.security.SecurityConfiguration)
	     */
	    SecurityContextHolder.getContext().setAuthentication(authentication);

	    /**
	     * UserDetailsService je interfejs koji sluzi za dobavljanje podataka o user-u,
	     * izgleda da ce ovde ipak biti injektovana nasa implementacija tog interfejsa
	     * (rs.ac.singidunum.banka.services.UserDetailsServiceImpl)
	     */
	    UserDetails details = studentService.loadUserByUsername(student.getId().toString());
	    String userToken = tokenUtils.generateToken(details);

	    HashMap<String, String> data = new HashMap<String, String>();
	    data.put("token", userToken);

	    return new ResponseEntity<HashMap<String, String>>(data, HttpStatus.OK);

	} catch (Exception e) {
	    return new ResponseEntity<HashMap<String, String>>(HttpStatus.UNAUTHORIZED);
	}
    }
}