package kolokvijum.plugin.studentskaSluzba.listener;

import java.time.LocalDateTime;

import javax.jms.Queue;
import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import kolokvijum.dto.BankTransaction;
import kolokvijum.hbsf.HibernateSessionFactory;
import kolokvijum.model.studentskaSluzba.FinansijskaKartica;
import kolokvijum.model.studentskaSluzba.Transakcija;
import kolokvijum.plugin.studentskaSluzba.service.FinansijskaKarticaService;
import kolokvijum.plugin.studentskaSluzba.service.TransakcijaService;

@Component
public class Listener {
    @Autowired
    private HibernateSessionFactory hbsf;
    
    @Autowired
    private TransakcijaService transakcijaService;
    
    @Autowired
    private FinansijskaKarticaService fService;
    
    @Autowired
    public Queue izvrsenaTransakcija;

    @Autowired
    private JmsTemplate jmsTemplate; // Podrazumevani jmsTemplate koji obezbedjuje Spring Boot.

    @Autowired
    private JmsTemplate jmsTopicTemplate; // Jms tempalte podesen za topic poruke

    /*
     * Ovaj listener je pretplacen na Topic pod nazivom incomingRequest. Potrebno je
     * koristiti fabriku za proizvodnju listenre-a za rad sa Topic porukama.
     * Podesavanje fabrike se vrsi podesavanjem vrednosti parametra containerFactory
     * u antoaciji. Vrednost ovog parametra je string koji predstavlja naziv factory
     * bean-a koji treba koristiti.
     */
    @JmsListener(destination = "izvrsenaTransakcija", containerFactory = "jmsQueueListenerContainerFactory")
    @Transactional
    public void onIncomingRequestEvent(BankTransaction bankTransaction) {
	System.out.println(bankTransaction.getPozivNaBroj());
	
	
	Session session = hbsf.getSession();
	session.beginTransaction();
	
	Query<FinansijskaKartica> query = session.createQuery("select f from FinansijskaKartica as f "
			+ "WHERE f.pozivNaBroj = :p", FinansijskaKartica.class);
	
	query.setParameter("p", bankTransaction.getPozivNaBroj());
	FinansijskaKartica fk = query.getSingleResult();
	
	fk.setStanje(fk.getStanje() + bankTransaction.getIznos());

	Transakcija t = new Transakcija();
	t.setDatumValute(bankTransaction.getDatumValute());
	t.setIznos(bankTransaction.getIznos());
	
	this.transakcijaService.save(t);
	
	this.fService.save(fk);
	
	session.getTransaction().commit();
	session.close();
    }

}
