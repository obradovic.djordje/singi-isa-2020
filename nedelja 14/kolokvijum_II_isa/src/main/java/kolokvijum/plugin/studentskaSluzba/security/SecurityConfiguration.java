package kolokvijum.plugin.studentskaSluzba.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import kolokvijum.plugin.studentskaSluzba.service.StudentService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private StudentService studentService;

    @Bean
    public PasswordEncoder getPasswordEncoder() {
	Map<String, PasswordEncoder> encoders = new HashMap<>();
	encoders.put("bcrypt", new BCryptPasswordEncoder());

	DelegatingPasswordEncoder passworEncoder = new DelegatingPasswordEncoder("bcrypt", encoders);
	passworEncoder.setDefaultPasswordEncoderForMatches(encoders.get("bcrypt"));
	return passworEncoder;
    }

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
	authenticationManagerBuilder.userDetailsService(this.studentService).passwordEncoder(this.getPasswordEncoder())
		.and().jdbcAuthentication();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
	return super.authenticationManagerBean();
    }

    @Bean
    public AuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
	AuthenticationTokenFilter authenticationTokenFilter = new AuthenticationTokenFilter();
	authenticationTokenFilter.setAuthenticationManager(this.authenticationManagerBean());
	return authenticationTokenFilter;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
	httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

	/**
	 * dodajemo filter koji ce se izvrsiti pri samom dolasku requesta na serversku
	 * stranu, pre pustanja njega da ide dalje(slicno middleware-u u node-u), u
	 * kojem cemo iz request-a pokusati da izvucemo vrednost za header
	 * Authorization(token vrednost) i na osnovu nje zakljuciti o kojem korisniku je
	 * rec i koje su njegove role, ako uopste ima prosledjen header Authorization,
	 * da vrednost nije null
	 */
	httpSecurity.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
	httpSecurity.csrf().disable();
    }
}
