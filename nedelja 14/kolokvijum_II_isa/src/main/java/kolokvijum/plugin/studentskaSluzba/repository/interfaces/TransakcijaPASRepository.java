package kolokvijum.plugin.studentskaSluzba.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokvijum.model.studentskaSluzba.Transakcija;

@Repository
@Scope("singleton")

public interface TransakcijaPASRepository extends PagingAndSortingRepository<Transakcija, Long> {

}