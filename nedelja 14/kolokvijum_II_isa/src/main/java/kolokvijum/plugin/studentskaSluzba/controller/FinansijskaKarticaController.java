package kolokvijum.plugin.studentskaSluzba.controller;

import java.util.HashMap;
import java.util.Optional;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolokvijum.controller.CrudController;
import kolokvijum.dto.studentskaSluzba.FinansijskaKarticaDTO;
import kolokvijum.model.studentskaSluzba.FinansijskaKartica;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/finansijska_kartica")
@CrossOrigin(origins = "*")

public class FinansijskaKarticaController extends CrudController<FinansijskaKartica, Long> {

    @Override
    @Secured("ROLE_REGISTROVANI_STUDENT")
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findOne(@PathVariable("id") Long id) {
	Optional<FinansijskaKartica> t = this.service.findOne(id);

	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

	if (t.isPresent()) {
	    User student = (User) authentication.getPrincipal();
	    if (student.getUsername().equals(t.get().getStudent().getId().toString())) {
		return new ResponseEntity<FinansijskaKarticaDTO>(t.get().getDTO(), HttpStatus.OK);
	    }

	    return new ResponseEntity<HashMap<String, String>>(HttpStatus.UNAUTHORIZED);

	} else {
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}

    }

}