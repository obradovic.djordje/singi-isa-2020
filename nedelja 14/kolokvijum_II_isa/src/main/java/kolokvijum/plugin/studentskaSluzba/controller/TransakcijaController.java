package kolokvijum.plugin.studentskaSluzba.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import kolokvijum.controller.CrudController;
import kolokvijum.model.studentskaSluzba.Transakcija;

@Controller
@Scope("singleton")
@RequestMapping(path = "api/transakcija")
@CrossOrigin(origins = "*")

public class TransakcijaController extends CrudController<Transakcija, Long> {

}