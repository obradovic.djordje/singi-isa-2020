package kolokvijum.plugin.studentskaSluzba.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration

public class PluginStudentskaSluzbaAppConfiguration {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
	properties.setLocation(new ClassPathResource(
		"/kolokvijum/plugin/studentskaSluzba/configuration/plugin.studentska.sluzba.application.properties"));
	properties.setIgnoreResourceNotFound(false);
	return properties;
    }

}