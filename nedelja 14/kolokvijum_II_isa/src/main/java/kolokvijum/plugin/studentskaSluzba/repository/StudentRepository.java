package kolokvijum.plugin.studentskaSluzba.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import kolokvijum.model.studentskaSluzba.Student;
import kolokvijum.repository.CrudRepository;

@Repository
@Scope("singleton")

public class StudentRepository extends CrudRepository<Student, Long> {

}