package kolokvijum.plugin.studentskaSluzba.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolokvijum.model.studentskaSluzba.Student;

@Repository
@Scope("singleton")

public interface StudentPASRepository extends PagingAndSortingRepository<Student, Long> {

}