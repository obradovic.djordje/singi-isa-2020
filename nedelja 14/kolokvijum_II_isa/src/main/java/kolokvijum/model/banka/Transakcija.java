package kolokvijum.model.banka;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import kolokvijum.dto.banka.TransakcijaDTO;
import kolokvijum.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "transakcija")
public class Transakcija implements Entitet<Transakcija, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -8600901113093058458L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "transakcija_id")
    private Long id;

    private Double iznos;

    private LocalDateTime datumValute;

    private String pozivNaBroj;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "racun_id")
    private Racun racun;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getIznos() {
	return this.iznos;
    }

    public void setIznos(Double iznos) {
	this.iznos = iznos;
    }

    public LocalDateTime getDatumValute() {
	return this.datumValute;
    }

    public void setDatumValute(LocalDateTime datumValute) {
	this.datumValute = datumValute;
    }

    public String getPozivNaBroj() {
	return this.pozivNaBroj;
    }

    public void setPozivNaBroj(String pozivNaBroj) {
	this.pozivNaBroj = pozivNaBroj;
    }

    public Racun getRacun() {
	return this.racun;
    }

    public void setRacun(Racun racun) {
	this.racun = racun;
    }

    public Transakcija() {
	super();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datumValute == null) ? 0 : datumValute.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((iznos == null) ? 0 : iznos.hashCode());
	result = prime * result + ((pozivNaBroj == null) ? 0 : pozivNaBroj.hashCode());
	result = prime * result + ((racun == null) ? 0 : racun.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Transakcija other = (Transakcija) obj;
	if (datumValute == null) {
	    if (other.datumValute != null)
		return false;
	} else if (!datumValute.equals(other.datumValute))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (iznos == null) {
	    if (other.iznos != null)
		return false;
	} else if (!iznos.equals(other.iznos))
	    return false;
	if (pozivNaBroj == null) {
	    if (other.pozivNaBroj != null)
		return false;
	} else if (!pozivNaBroj.equals(other.pozivNaBroj))
	    return false;
	if (racun == null) {
	    if (other.racun != null)
		return false;
	} else if (!racun.equals(other.racun))
	    return false;
	return true;
    }

    @Override
    public TransakcijaDTO getDTO() {
	TransakcijaDTO transakcijaDTO = new TransakcijaDTO();
	transakcijaDTO.setId(id);
	transakcijaDTO.setDatumValute(datumValute);
	transakcijaDTO.setIznos(iznos);
	transakcijaDTO.setPozivNaBroj(pozivNaBroj);

	if (this.racun != null)
	    transakcijaDTO.setRacunDTO(this.racun.getDTOinsideDTO());

	return transakcijaDTO;
    }

    @Override
    public TransakcijaDTO getDTOinsideDTO() {
	TransakcijaDTO transakcijaDTO = new TransakcijaDTO();
	transakcijaDTO.setId(id);
	transakcijaDTO.setDatumValute(datumValute);
	transakcijaDTO.setIznos(iznos);
	transakcijaDTO.setPozivNaBroj(pozivNaBroj);
	return transakcijaDTO;
    }

    @Override
    public void update(Transakcija entitet) {
	this.setDatumValute(entitet.getDatumValute());
	this.setIznos(entitet.getIznos());
	this.setPozivNaBroj(entitet.getPozivNaBroj());

	this.setRacun(entitet.getRacun());

    }

}