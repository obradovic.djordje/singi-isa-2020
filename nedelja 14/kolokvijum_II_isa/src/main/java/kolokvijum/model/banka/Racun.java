package kolokvijum.model.banka;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Table;

import kolokvijum.dto.banka.RacunDTO;
import kolokvijum.dto.banka.TransakcijaDTO;
import kolokvijum.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "racun")
public class Racun implements Entitet<Racun, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 7269026931772556544L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "racun_id")
    private Long id;

    private String brojRacuna;

    private Double stanje;

    @javax.persistence.OneToMany(mappedBy = "racun")
    private java.util.List<Transakcija> transakcije;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getBrojRacuna() {
	return this.brojRacuna;
    }

    public void setBrojRacuna(String brojRacuna) {
	this.brojRacuna = brojRacuna;
    }

    public Double getStanje() {
	return this.stanje;
    }

    public void setStanje(Double stanje) {
	this.stanje = stanje;
    }

    public java.util.List<Transakcija> getTransakcije() {
	return this.transakcije;
    }

    public void setTransakcije(java.util.List<Transakcija> transakcije) {
	this.transakcije = transakcije;
    }

    public Racun() {
	super();
	this.transakcije = new java.util.ArrayList<>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((brojRacuna == null) ? 0 : brojRacuna.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((transakcije == null) ? 0 : transakcije.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Racun other = (Racun) obj;
	if (brojRacuna == null) {
	    if (other.brojRacuna != null)
		return false;
	} else if (!brojRacuna.equals(other.brojRacuna))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (stanje == null) {
	    if (other.stanje != null)
		return false;
	} else if (!stanje.equals(other.stanje))
	    return false;
	if (transakcije == null) {
	    if (other.transakcije != null)
		return false;
	} else if (!transakcije.equals(other.transakcije))
	    return false;
	return true;
    }

    @Override
    public RacunDTO getDTO() {
	RacunDTO racunDTO = new RacunDTO();
	racunDTO.setId(id);
	racunDTO.setBrojRacuna(brojRacuna);
	racunDTO.setStanje(stanje);

	ArrayList<TransakcijaDTO> transakcijeDTOs = new ArrayList<TransakcijaDTO>();
	for (Transakcija transakcija : this.transakcije) {
	    transakcijeDTOs.add(transakcija.getDTOinsideDTO());
	}
	racunDTO.setTransakcijeDTO(transakcijeDTOs);

	return racunDTO;
    }

    @Override
    public RacunDTO getDTOinsideDTO() {
	RacunDTO racunDTO = new RacunDTO();
	racunDTO.setId(id);
	racunDTO.setBrojRacuna(brojRacuna);
	racunDTO.setStanje(stanje);
	return racunDTO;
    }

    @Override
    public void update(Racun entitet) {
	this.setBrojRacuna(entitet.getBrojRacuna());
	this.setStanje(entitet.getStanje());

    }

}