package kolokvijum.model.studentskaSluzba;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import kolokvijum.dto.studentskaSluzba.FinansijskaKarticaDTO;
import kolokvijum.dto.studentskaSluzba.TransakcijaDTO;
import kolokvijum.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "finansijska_kartica")
public class FinansijskaKartica implements Entitet<FinansijskaKartica, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 6916988650544446465L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "finansijska_kartica_id")
    private Long id;

    private String pozivNaBroj;

    private Double stanje;

//    @javax.persistence.OneToOne(mappedBy = "finansijskaKartica")
    @OneToOne
    private Student student;

    @javax.persistence.OneToMany(mappedBy = "finansijskaKartica")
    private java.util.List<Transakcija> transakcije;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getPozivNaBroj() {
	return this.pozivNaBroj;
    }

    public void setPozivNaBroj(String pozivNaBroj) {
	this.pozivNaBroj = pozivNaBroj;
    }

    public Double getStanje() {
	return this.stanje;
    }

    public void setStanje(Double stanje) {
	this.stanje = stanje;
    }

    public Student getStudent() {
	return this.student;
    }

    public void setStudent(Student student) {
	this.student = student;
    }

    public java.util.List<Transakcija> getTransakcije() {
	return this.transakcije;
    }

    public void setTransakcije(java.util.List<Transakcija> transakcije) {
	this.transakcije = transakcije;
    }

    public FinansijskaKartica() {
	super();
	this.transakcije = new java.util.ArrayList<>();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((pozivNaBroj == null) ? 0 : pozivNaBroj.hashCode());
	result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
	result = prime * result + ((student == null) ? 0 : student.hashCode());
	result = prime * result + ((transakcije == null) ? 0 : transakcije.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	FinansijskaKartica other = (FinansijskaKartica) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (pozivNaBroj == null) {
	    if (other.pozivNaBroj != null)
		return false;
	} else if (!pozivNaBroj.equals(other.pozivNaBroj))
	    return false;
	if (stanje == null) {
	    if (other.stanje != null)
		return false;
	} else if (!stanje.equals(other.stanje))
	    return false;
	if (student == null) {
	    if (other.student != null)
		return false;
	} else if (!student.equals(other.student))
	    return false;
	if (transakcije == null) {
	    if (other.transakcije != null)
		return false;
	} else if (!transakcije.equals(other.transakcije))
	    return false;
	return true;
    }

    @Override
    public FinansijskaKarticaDTO getDTO() {
	FinansijskaKarticaDTO finansijskaKarticaDTO = new FinansijskaKarticaDTO();
	finansijskaKarticaDTO.setId(id);
	finansijskaKarticaDTO.setStanje(stanje);
	finansijskaKarticaDTO.setPozivNaBroj(pozivNaBroj);

	if (this.student != null)
	    finansijskaKarticaDTO.setStudentDTO(this.student.getDTOinsideDTO());

	ArrayList<TransakcijaDTO> transakcijeDTOs = new ArrayList<TransakcijaDTO>();
	for (Transakcija transakcija : this.transakcije) {
	    transakcijeDTOs.add(transakcija.getDTOinsideDTO());
	}
	finansijskaKarticaDTO.setTransakcijeDTO(transakcijeDTOs);

	return finansijskaKarticaDTO;
    }

    @Override
    public FinansijskaKarticaDTO getDTOinsideDTO() {
	FinansijskaKarticaDTO finansijskaKarticaDTO = new FinansijskaKarticaDTO();
	finansijskaKarticaDTO.setId(id);
	finansijskaKarticaDTO.setStanje(stanje);
	finansijskaKarticaDTO.setPozivNaBroj(pozivNaBroj);
	return finansijskaKarticaDTO;
    }

    @Override
    public void update(FinansijskaKartica entitet) {
	this.setStanje(entitet.getStanje());
	this.setPozivNaBroj(entitet.getPozivNaBroj());

	this.setStudent(entitet.getStudent());
    }

}