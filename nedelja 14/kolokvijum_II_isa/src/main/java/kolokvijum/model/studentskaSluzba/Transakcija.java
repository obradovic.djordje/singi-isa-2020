package kolokvijum.model.studentskaSluzba;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import kolokvijum.dto.studentskaSluzba.TransakcijaDTO;
import kolokvijum.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "transakcija")
public class Transakcija implements Entitet<Transakcija, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 1697706286980546134L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "transakcija_id")
    private Long id;

    private Double iznos;

    private LocalDateTime datumValute;

    @javax.persistence.ManyToOne
    @JoinColumn(name = "finansijska_kartica_id")
    private FinansijskaKartica finansijskaKartica;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getIznos() {
	return this.iznos;
    }

    public void setIznos(Double iznos) {
	this.iznos = iznos;
    }

    public LocalDateTime getDatumValute() {
	return this.datumValute;
    }

    public void setDatumValute(LocalDateTime datumValute) {
	this.datumValute = datumValute;
    }

    public FinansijskaKartica getFinansijskaKartica() {
	return this.finansijskaKartica;
    }

    public void setFinansijskaKartica(FinansijskaKartica finansijskaKartica) {
	this.finansijskaKartica = finansijskaKartica;
    }

    public Transakcija() {
	super();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((datumValute == null) ? 0 : datumValute.hashCode());
	result = prime * result + ((finansijskaKartica == null) ? 0 : finansijskaKartica.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((iznos == null) ? 0 : iznos.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Transakcija other = (Transakcija) obj;
	if (datumValute == null) {
	    if (other.datumValute != null)
		return false;
	} else if (!datumValute.equals(other.datumValute))
	    return false;
	if (finansijskaKartica == null) {
	    if (other.finansijskaKartica != null)
		return false;
	} else if (!finansijskaKartica.equals(other.finansijskaKartica))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (iznos == null) {
	    if (other.iznos != null)
		return false;
	} else if (!iznos.equals(other.iznos))
	    return false;
	return true;
    }

    @Override
    public TransakcijaDTO getDTO() {
	TransakcijaDTO transakcijaDTO = new TransakcijaDTO();
	transakcijaDTO.setId(id);
	transakcijaDTO.setDatumValute(datumValute);
	transakcijaDTO.setIznos(iznos);

	if (this.finansijskaKartica != null)
	    transakcijaDTO.setFinansijskaKarticaDTO(this.finansijskaKartica.getDTOinsideDTO());

	return transakcijaDTO;
    }

    @Override
    public TransakcijaDTO getDTOinsideDTO() {
	TransakcijaDTO transakcijaDTO = new TransakcijaDTO();
	transakcijaDTO.setId(id);
	transakcijaDTO.setDatumValute(datumValute);
	transakcijaDTO.setIznos(iznos);
	return transakcijaDTO;
    }

    @Override
    public void update(Transakcija entitet) {
	this.setDatumValute(entitet.getDatumValute());
	this.setIznos(entitet.getIznos());

	this.setFinansijskaKartica(entitet.getFinansijskaKartica());

    }

}