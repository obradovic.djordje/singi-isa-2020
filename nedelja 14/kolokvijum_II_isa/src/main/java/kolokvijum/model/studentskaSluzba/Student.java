package kolokvijum.model.studentskaSluzba;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import kolokvijum.dto.studentskaSluzba.StudentDTO;
import kolokvijum.model.interfaces.Entitet;

@javax.persistence.Entity

@Table(name = "student")
public class Student implements Entitet<Student, Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3460209389578603395L;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long id;

    private String indeks;

    private String ime;

    private String prezime;

    private String email;

    private String lozinka;

    @javax.persistence.OneToOne(mappedBy = "student")
//    @OneToOne
    private FinansijskaKartica finansijskaKartica;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private Set<StudentRola> studentRole;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getIndeks() {
	return this.indeks;
    }

    public void setIndeks(String indeks) {
	this.indeks = indeks;
    }

    public String getIme() {
	return this.ime;
    }

    public void setIme(String ime) {
	this.ime = ime;
    }

    public String getPrezime() {
	return this.prezime;
    }

    public void setPrezime(String prezime) {
	this.prezime = prezime;
    }

    public String getEmail() {
	return this.email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getLozinka() {
	return this.lozinka;
    }

    public void setLozinka(String lozinka) {
	this.lozinka = lozinka;
    }

    public FinansijskaKartica getFinansijskaKartica() {
	return this.finansijskaKartica;
    }

    public void setFinansijskaKartica(FinansijskaKartica finansijskaKartica) {
	this.finansijskaKartica = finansijskaKartica;
    }

    public Set<StudentRola> getStudentRole() {
	return studentRole;
    }

    public void setStudentRole(Set<StudentRola> studentRole) {
	this.studentRole = studentRole;
    }

    public Student() {
	super();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((email == null) ? 0 : email.hashCode());
	result = prime * result + ((finansijskaKartica == null) ? 0 : finansijskaKartica.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((ime == null) ? 0 : ime.hashCode());
	result = prime * result + ((indeks == null) ? 0 : indeks.hashCode());
	result = prime * result + ((lozinka == null) ? 0 : lozinka.hashCode());
	result = prime * result + ((prezime == null) ? 0 : prezime.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Student other = (Student) obj;
	if (email == null) {
	    if (other.email != null)
		return false;
	} else if (!email.equals(other.email))
	    return false;
	if (finansijskaKartica == null) {
	    if (other.finansijskaKartica != null)
		return false;
	} else if (!finansijskaKartica.equals(other.finansijskaKartica))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (ime == null) {
	    if (other.ime != null)
		return false;
	} else if (!ime.equals(other.ime))
	    return false;
	if (indeks == null) {
	    if (other.indeks != null)
		return false;
	} else if (!indeks.equals(other.indeks))
	    return false;
	if (lozinka == null) {
	    if (other.lozinka != null)
		return false;
	} else if (!lozinka.equals(other.lozinka))
	    return false;
	if (prezime == null) {
	    if (other.prezime != null)
		return false;
	} else if (!prezime.equals(other.prezime))
	    return false;
	return true;
    }

    @Override
    public StudentDTO getDTO() {
	StudentDTO studentDTO = new StudentDTO();
	studentDTO.setId(id);
	studentDTO.setEmail(email);
	studentDTO.setIme(ime);
	studentDTO.setIndeks(indeks);
	studentDTO.setLozinka(lozinka);
	studentDTO.setPrezime(prezime);

	if (this.finansijskaKartica != null)
	    studentDTO.setFinansijskaKarticaDTO(this.finansijskaKartica.getDTOinsideDTO());

	return studentDTO;
    }

    @Override
    public StudentDTO getDTOinsideDTO() {
	StudentDTO studentDTO = new StudentDTO();
	studentDTO.setId(id);
	studentDTO.setEmail(email);
	studentDTO.setIme(ime);
	studentDTO.setIndeks(indeks);
	studentDTO.setLozinka(lozinka);
	studentDTO.setPrezime(prezime);
	return studentDTO;
    }

    @Override
    public void update(Student entitet) {
	this.setEmail(entitet.getEmail());
	this.setIme(entitet.getIme());
	this.setIndeks(entitet.getIndeks());
	this.setLozinka(entitet.getLozinka());
	this.setPrezime(entitet.getPrezime());

	this.setFinansijskaKartica(entitet.getFinansijskaKartica());

    }

}