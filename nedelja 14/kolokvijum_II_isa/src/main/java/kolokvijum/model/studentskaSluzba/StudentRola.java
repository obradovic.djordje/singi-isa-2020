package kolokvijum.model.studentskaSluzba;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class StudentRola {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Student student;

    @ManyToOne
    private Rola rola;

    public StudentRola() {
    }

    public StudentRola(Long id, Student student, Rola rola) {
	super();
	this.id = id;
	this.student = student;
	this.rola = rola;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Student getStudent() {
	return student;
    }

    public void setStudent(Student student) {
	this.student = student;
    }

    public Rola getRola() {
	return rola;
    }

    public void setRola(Rola rola) {
	this.rola = rola;
    }

}
