package kolokvijum.model.interfaces;

import java.io.Serializable;

import kolokvijum.dto.interfaces.DTO;
import kolokvijum.interfaces.Identifikacija;

public interface Entitet<T, ID extends Serializable> extends Identifikacija<ID> {

    public DTO<ID> getDTO();

    public DTO<ID> getDTOinsideDTO();

    public void update(T entitet);

}