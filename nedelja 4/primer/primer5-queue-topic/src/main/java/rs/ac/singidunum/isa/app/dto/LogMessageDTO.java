package rs.ac.singidunum.isa.app.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class LogMessageDTO implements Serializable {
	private static final long serialVersionUID = -5008238127430475320L;
	private String messageTitle;
	private String messageContent;
	private LocalDateTime datetime;

	public LogMessageDTO() {
		super();
	}

	public LogMessageDTO(String messageTitle, String messageContent, LocalDateTime datetime) {
		super();
		this.messageTitle = messageTitle;
		this.messageContent = messageContent;
		this.datetime = datetime;
	}

	public String getMessageTitle() {
		return messageTitle;
	}

	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public LocalDateTime getDatetime() {
		return datetime;
	}

	public void setDatetime(LocalDateTime datetime) {
		this.datetime = datetime;
	}

	@Override
	public String toString() {
		return "LogMessageDTO [messageTitle=" + messageTitle + ", messageContent=" + messageContent + ", datetime="
				+ datetime + "]";
	}
}
