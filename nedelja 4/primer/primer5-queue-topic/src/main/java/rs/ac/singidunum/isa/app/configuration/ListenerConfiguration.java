package rs.ac.singidunum.isa.app.configuration;

import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@Configuration
public class ListenerConfiguration {
	@Autowired
	ConnectionFactory connectionFactory;
	
	/*
	 * Definisanje bean-a za topic listener fabriku.
	 */
	@Bean
	public DefaultJmsListenerContainerFactory jmsTopicListenerContainerFactory(
			DefaultJmsListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		configurer.configure(factory, connectionFactory);
		factory.setPubSubDomain(true);
		return factory;
	}
	
	/*
	 * Definisanje jms template bean-a za queue poruke.
	 */
	@Bean
	public JmsTemplate jmsTemplate() {
		JmsTemplate template = new JmsTemplate(connectionFactory);
		template.setPubSubDomain(false);
		return template;
	}
	
	/*
	 * Definisanje jms template bean-a za topic poruke.
	 */
	@Bean
	public JmsTemplate jmsTopicTemplate() {
		JmsTemplate template = new JmsTemplate(connectionFactory);
		template.setPubSubDomain(true);
		return template;
	}
}
