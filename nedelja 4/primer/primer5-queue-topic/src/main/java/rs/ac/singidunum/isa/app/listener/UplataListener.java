package rs.ac.singidunum.isa.app.listener;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.LogMessageDTO;

@Component
public class UplataListener {
	@Autowired
	private JmsTemplate jmsTemplate; // Podrazumevani jmsTemplate koji obezbedjuje Spring Boot.
	
	
	@Autowired
	private JmsTemplate jmsTopicTemplate; // Jms tempalte podesen za topic poruke.
	
	/*
	 * Ovaj listener je pretplacen na Topic pod nazivom uplate.
	 * Potrebno je koristiti fabriku za proizvodnju listenre-a
	 * za rad sa Topic porukama. Podesavanje fabrike se vrsi
	 * podesavanjem vrednosti parametra containerFactory u
	 * antoaciji. Vrednost ovog parametra je string koji
	 * predstavlja naziv factory bean-a koji treba koristiti.
	 */
	@JmsListener(destination = "uplate", containerFactory = "jmsTopicListenerContainerFactory")
	@SendTo("potvrde")
	public String onUplataEvent(LogMessageDTO message) {
		System.out.println(message);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		jmsTemplate.convertAndSend("logs", new LogMessageDTO("Jms template queue primer", "Slanje point to point poruke!", LocalDateTime.now()));
		jmsTopicTemplate.convertAndSend("logs", new LogMessageDTO("Neispravno", "Ne moze se slati jer je template konfigurisan da salje poruke za topic.", LocalDateTime.now()));
		jmsTopicTemplate.convertAndSend("potvrde", "POSLATO - JMS TOPIC TEMPLATE");
		return "POSLATO";
	}
}
