package rs.ac.singidunum.isa.app.configuration;

import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfiguration {
	@Bean
	public Topic uplateTopic() {
		return new ActiveMQTopic("potvrde");
	}
}
