package rs.ac.singidunum.isa.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.LogMessageDTO;

@Component
public class LogListener {
	/*
	 * Ako se ne navede container factory koristice se
	 * podrazumevani factory koji podrzava samo point to point
	 * poruke, odnosno rad sa porukama za queue.
	 */
	@JmsListener(destination = "logs")
	public void onLogEvent(LogMessageDTO message) {
		System.out.println(message);
	}
}
