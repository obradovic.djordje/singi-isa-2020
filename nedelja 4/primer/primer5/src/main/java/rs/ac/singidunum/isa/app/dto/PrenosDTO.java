package rs.ac.singidunum.isa.app.dto;

import java.time.LocalDateTime;

import rs.ac.singidunum.isa.app.model.Prenos;

public class PrenosDTO {
	private String uplatilac;
	private String primalac;
	private Double iznos;
	private LocalDateTime datumUplate;

	public PrenosDTO() {
		super();
	}

	public PrenosDTO(Prenos prenos) {
		this.uplatilac = prenos.getUplatilac().getBrojRacuna();
		this.primalac = prenos.getPrimalac().getBrojRacuna();
		this.iznos = prenos.getIznos();
		this.datumUplate = prenos.getDatumUplate();
	}
	
	public PrenosDTO(String uplatilac, String primalac, Double iznos, LocalDateTime datumUplate) {
		super();
		this.uplatilac = uplatilac;
		this.primalac = primalac;
		this.iznos = iznos;
		this.datumUplate = datumUplate;
	}

	public String getUplatilac() {
		return uplatilac;
	}

	public void setUplatilac(String uplatilac) {
		this.uplatilac = uplatilac;
	}

	public String getPrimalac() {
		return primalac;
	}

	public void setPrimalac(String primalac) {
		this.primalac = primalac;
	}

	public Double getIznos() {
		return iznos;
	}

	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}

	public LocalDateTime getDatumUplate() {
		return datumUplate;
	}

	public void setDatumUplate(LocalDateTime datumUplate) {
		this.datumUplate = datumUplate;
	}

	@Override
	public String toString() {
		return "PrenosDTO [uplatilac=" + uplatilac + ", primalac=" + primalac + ", iznos=" + iznos + "]";
	}

}
