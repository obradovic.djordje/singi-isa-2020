package rs.ac.singidunum.isa.app.configuration;

import javax.jms.Queue;
import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfiguration {
	@Bean
	public Queue logQueue() {
		return new ActiveMQQueue("logs");
	}
	
	@Bean
	public Topic uplateTopic() {
		return new ActiveMQTopic("uplate");
	}
}
