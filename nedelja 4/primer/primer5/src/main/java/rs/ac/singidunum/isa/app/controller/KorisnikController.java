package rs.ac.singidunum.isa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.KorisnikDTO;
import rs.ac.singidunum.isa.app.service.KorisnikService;

@Controller
@RequestMapping(path = "/api/korisnici")
@CrossOrigin
public class KorisnikController {
	@Autowired
	private KorisnikService korisnikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<KorisnikDTO>> getAllKorisnici(Pageable pageable) {
		return new ResponseEntity<Page<KorisnikDTO>>(korisnikService.findAll(pageable).map(k->new KorisnikDTO(k)), HttpStatus.OK);
	}
}
