package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.List;

import rs.ac.singidunum.isa.app.model.Prenos;
import rs.ac.singidunum.isa.app.model.Racun;

public class RacunDTO {
	private String brojRacuna;
	private Double stanje;
	private KorisnikDTO vlasnik;
	private List<PrenosDTO> uplate = new ArrayList<PrenosDTO>();
	private List<PrenosDTO> isplate = new ArrayList<PrenosDTO>();
	
	public RacunDTO() {
		super();
	}
	
	public RacunDTO(Racun racun) {
		this.brojRacuna = racun.getBrojRacuna();
		this.stanje = racun.getStanje();
		this.vlasnik = new KorisnikDTO(racun.getVlasnik().getId(), racun.getVlasnik().getKorisnickoIme(), racun.getVlasnik().getIme(), racun.getVlasnik().getPrezime());
		for(Prenos u : racun.getUplate()) {
			uplate.add(new PrenosDTO(u));
		}
		for(Prenos i : racun.getIsplate()) {
			isplate.add(new PrenosDTO(i));
		}
	}

	public RacunDTO(String brojRacuna, Double stanje, KorisnikDTO vlasnik) {
		super();
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
		this.vlasnik = vlasnik;
	}

	public String getBrojRacuna() {
		return brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public KorisnikDTO getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(KorisnikDTO vlasnik) {
		this.vlasnik = vlasnik;
	}

	public List<PrenosDTO> getUplate() {
		return uplate;
	}

	public void setUplate(List<PrenosDTO> uplate) {
		this.uplate = uplate;
	}

	public List<PrenosDTO> getIsplate() {
		return isplate;
	}

	public void setIsplate(List<PrenosDTO> isplate) {
		this.isplate = isplate;
	}
}
