package rs.ac.singidunum.isa.app.aspect;

import java.time.LocalDateTime;

import javax.jms.Queue;
import javax.jms.Topic;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.LogMessageDTO;
import rs.ac.singidunum.isa.app.dto.PrenosDTO;

@Aspect
@Component
public class PrenosLogger {
	@Autowired
    private Queue logQueue;
	
	@Autowired
	private Topic uplateTopic;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Before("@annotation(Logged)")
	public void logPocetak(JoinPoint jp) {
		System.out.println("Zapoceto izvrsavanje metode: ");
		System.out.println(jp.getSignature());
		System.out.println("Argumenti metode: ");
		for(Object o : jp.getArgs()) {
			System.out.println(o);
		}
		System.out.println("---------------");
//		jmsTemplate.convertAndSend(logQueue, "Zapoceto izvrsavanje metode "+jp.getSignature());
		jmsTemplate.convertAndSend(logQueue, new LogMessageDTO("Before execution", "Metoda "+jp.getSignature(), LocalDateTime.now()));
	}
	
	@Around("execution(* *.createUplata(..))")
	public void logOkoPrenosa(ProceedingJoinPoint jp) {
		System.out.println("Pocetak izvrsavanja: ");
		System.out.println(jp.getSignature());
		try {
			Object result = jp.proceed();
			System.out.println("Rezultat izvrsavanja: ");
			System.out.println(result);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("Kraj izvrsavanja: ");
		System.out.println(jp.getSignature());
		System.out.println("------------------");
		jmsTemplate.convertAndSend(logQueue, new LogMessageDTO("Around execution", "Metoda "+jp.getSignature(), LocalDateTime.now()));
	}
	
	@After("execution(* *.createUplata(rs.ac.singidunum.isa.app.dto.PrenosDTO)) && args(prenos,..)")
	public void logKrajPrenosa(PrenosDTO prenos) {
		System.out.println("Zavrsen prenos.");
		System.out.println(prenos);
		System.out.println("---------------");
//		jmsTemplate.convertAndSend(logQueue, "Zavrsen prenos");
		jmsTemplate.convertAndSend(logQueue, new LogMessageDTO("After execution", "Metoda zavrsena", LocalDateTime.now()));
		jmsTemplate.convertAndSend(uplateTopic, new LogMessageDTO("Nakon uplate", "Uplata izvrsena!", LocalDateTime.now()));
	}
}
