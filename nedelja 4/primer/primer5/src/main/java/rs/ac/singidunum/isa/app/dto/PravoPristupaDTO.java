package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.List;

import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.model.PravoPristupa;

public class PravoPristupaDTO {
	private Long id;
	private String naziv;
	private List<KorisnikDTO> korisnici = new ArrayList<KorisnikDTO>();

	public PravoPristupaDTO() {
		super();
	}

	public PravoPristupaDTO(PravoPristupa pravoPristupa) {
		super();

		this.id = pravoPristupa.getId();
		this.naziv = pravoPristupa.getNaziv();
		for (Korisnik k : pravoPristupa.getKorisnici()) {
			korisnici.add(new KorisnikDTO(k.getId(), k.getKorisnickoIme(), k.getKorisnickoIme(), k.getPrezime()));
		}
	}

	public PravoPristupaDTO(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<KorisnikDTO> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(List<KorisnikDTO> korisnici) {
		this.korisnici = korisnici;
	}
}
