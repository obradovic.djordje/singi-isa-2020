package rs.ac.singidunum.isa.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class PotvrdaListener {
	@JmsListener(destination = "potvrde")
	public void onUplataEvent(String potvrda) {
		System.out.println(potvrda);
	}
}
