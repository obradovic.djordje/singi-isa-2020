package rs.ac.singidunum.isa.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Prenos {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private Racun uplatilac;
	@ManyToOne
	private Racun primalac;

	@Column(nullable = false)
	private LocalDateTime datumUplate;

	@Column(nullable = false)
	private Double iznos;

	public Prenos() {
		super();
	}

	public Prenos(Long id, Racun uplatilac, Racun primalac, LocalDateTime datumUplate, Double iznos) {
		super();
		this.id = id;
		this.uplatilac = uplatilac;
		this.primalac = primalac;
		this.datumUplate = datumUplate;
		this.iznos = iznos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Racun getUplatilac() {
		return uplatilac;
	}

	public void setUplatilac(Racun uplatilac) {
		this.uplatilac = uplatilac;
	}

	public Racun getPrimalac() {
		return primalac;
	}

	public void setPrimalac(Racun primalac) {
		this.primalac = primalac;
	}

	public LocalDateTime getDatumUplate() {
		return datumUplate;
	}

	public void setDatumUplate(LocalDateTime datumUplate) {
		this.datumUplate = datumUplate;
	}

	public Double getIznos() {
		return iznos;
	}

	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prenos other = (Prenos) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
