package rs.ac.singidunum.isa.app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Prenos;

@Repository
public interface PrenosRepository extends PagingAndSortingRepository<Prenos, Long> {

}
