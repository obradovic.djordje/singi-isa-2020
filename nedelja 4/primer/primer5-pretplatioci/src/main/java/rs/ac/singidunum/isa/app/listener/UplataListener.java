package rs.ac.singidunum.isa.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.LogMessageDTO;

@Component
public class UplataListener {
	@JmsListener(destination = "uplate")
	@SendTo("potvrde")
	public String onUplataEvent(LogMessageDTO message) {
		System.out.println(message);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "POSLATO";
	}
}
