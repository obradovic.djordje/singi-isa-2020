package rs.ac.singidunum.isa.app.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class LogMessage {
	@Id
	private String id;
	private String messageTitle;
	private String messageContent;
	private LocalDateTime datetime;

	public LogMessage() {
		super();
	}

	public LogMessage(String id, String messageTitle, String messageContent, LocalDateTime datetime) {
		super();
		this.id = id;
		this.messageTitle = messageTitle;
		this.messageContent = messageContent;
		this.datetime = datetime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessageTitle() {
		return messageTitle;
	}

	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public LocalDateTime getDatetime() {
		return datetime;
	}

	public void setDatetime(LocalDateTime datetime) {
		this.datetime = datetime;
	}
}
