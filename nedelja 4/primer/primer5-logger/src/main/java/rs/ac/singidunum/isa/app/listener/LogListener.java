package rs.ac.singidunum.isa.app.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.isa.app.dto.LogMessageDTO;
import rs.ac.singidunum.isa.app.model.LogMessage;
import rs.ac.singidunum.isa.app.repository.LogMessageRepository;

@Component
public class LogListener {
	@Autowired
	LogMessageRepository lmr;
//	@JmsListener(destination = "logs")
//	public void onLogEvent(String message) {
//		System.out.println(message);
//	}
	
	@JmsListener(destination = "logs")
	public void onLogEvent(LogMessageDTO message) {
		lmr.save(new LogMessage(null, message.getMessageTitle(), message.getMessageContent(), message.getDatetime()));
		System.out.println(message);
	}
}
